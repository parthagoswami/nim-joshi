import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InteractiveSearchComponent } from './interactive-search.component';

describe('InteractiveSearchComponent', () => {
  let component: InteractiveSearchComponent;
  let fixture: ComponentFixture<InteractiveSearchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InteractiveSearchComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InteractiveSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
