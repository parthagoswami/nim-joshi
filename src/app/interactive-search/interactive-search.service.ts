import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class InteractiveSearchService {

  constructor(private _http:HttpClient) { }
  getSearchSettingData(){
    return this._http.get(environment.apiBaseUrl + 'cms/interactive-search')
    .pipe(map(res => JSON.parse(JSON.stringify(res))));

  }
  getFilterCareerSearchOptions(accessToken: string, jobSector: any) {
    let fd = new FormData();
    fd.append("job_sector", jobSector);
    let headers = new HttpHeaders()
    //.set('Authorization', 'Bearer ' + accessToken);
    

    return this._http.post(environment.apiBaseUrl + 'get-searchresult-filter-options', fd, { headers })
    .pipe(map(res => JSON.parse(JSON.stringify(res))));
  }
  getCareerSearchData(accessToken: string, jobLevel: any, jobSector:any,minWageSlider:any,maxWageSlider:any)
  {
    //console.log(minWage);
    //console.log('test');
    let fd = new FormData();
   
    fd.append("sector_id", jobSector);
    fd.append("job_level_id", jobLevel);
    fd.append("min_wage", minWageSlider);
    fd.append("max_wage", maxWageSlider);
   
  
    //let page_no = pageNo;
    //console.log(pageNo);
    // fd.append("page", ''+pageNo);

    let headers = new HttpHeaders()
    //.set('Authorization', 'Bearer ' + accessToken);

    return this._http.post(environment.apiBaseUrl + 'searchresult-interactive', fd, { headers })
    .pipe(map(res => JSON.parse(JSON.stringify(res))));

  }
  checkLoginState(apiKey: string,accessToken: string){
    let fd = new FormData();

    let headers = new HttpHeaders()
    .set('Authorization', 'Bearer ' + accessToken);

    return this._http.post(environment.apiBaseUrl + 'auth/is-logged-in', fd, { headers: headers })
    .pipe(map(res => JSON.parse(JSON.stringify(res))));
  }
}
