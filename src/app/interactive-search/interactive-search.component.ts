import { Component, OnInit } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AfterLoginService } from '../after-login/after-login.service';
import { InteractiveSearchService } from './interactive-search.service';
import { environment } from '../../environments/environment';
declare var $:any;
var px_block_width = 202;
var px_dotborder_width = 4;
var px_tooltip_arrow_width = 20;
var px_line_width = 20;
var bool_mobile = false;
var bool_touch = false;
var bool_ie = false;
var array_timeouts = new Array();

@Component({
  selector: 'app-interactive-search',
  templateUrl: './interactive-search.component.html',
  styleUrls: ['./interactive-search.component.css']
})
export class InteractiveSearchComponent implements OnInit {
  public accessToken: string = '';
  //public settingData: any = [];
  public apiFileUrl: string = environment.apiFileUrl;
  public pageData: any = [];
  public bannerUrl: any = [];
  public bannerCaption: any = [];
  public bannerAlt: string = '';
  public isLoggedIn : boolean=false;
  public userIdNo : any = [];
  public fliterWageMin:  number = 0;
  public filterWageMax:  number = 0;
  public filterData: any = [];
  public careerData: any = [];
  public jobLevel: any = [];
  public jobSector: any = [];
  public minWageSlider:  any = '';
  public maxWageSlider:  any = '';
  public currentPage: number = 1;
  public selectedJobLevel: string='';
  public selectedJobSector: string='';
  public selectedWage: string='';
  public jobSectorName: any = [];
  public jobSectorsMaster: any = [];
  public opera: any = [];
  public userRole: any;
  public apiKey: string = environment.apiKey;
  public isLoggedInchk : boolean = false;
  constructor(private _intsearchService:InteractiveSearchService,private _afterLoginService:AfterLoginService,private _router:Router,private _title:Title,private _toastr:ToastrService,private meta: Meta) { 
    let accessToken = localStorage.getItem(btoa('access_token'));
    if(typeof accessToken !== 'undefined' && accessToken != null && accessToken != '') {
      this.accessToken = atob(accessToken);
    }
    this.checkLogeedIn();
    
  }

  ngOnInit(): void {
    try{
      let accessToken = localStorage.getItem(btoa('access_token'));
     
      if(typeof accessToken !== 'undefined' && accessToken != null && accessToken != '') {
        this.accessToken = atob(accessToken); 
        this.isLoggedIn = true;
      }
      else
      {
        localStorage.removeItem('user_id');
      }
      let userId = localStorage.getItem('user_id');
     
      if(typeof userId !== 'undefined' && userId != null && userId != '')
      {
        this.userIdNo = btoa(userId);
      }
      this.jobSector = localStorage.getItem('jbSector');
      this.jobSectorName = localStorage.getItem('jbSectorName');
      
      //this.initJquery();
      this.getSettingData();
      this.getFilterCareerSearch();
    }catch(error){
      console.log(error);
    }
    //setTimeout(() =>{ $(".lazy").lazy(); }, 1000);
  }
  
  getSettingData(){    
    this._intsearchService.getSearchSettingData()
    .subscribe((resp)=> {      
      if(resp.status == 200) {
        this.pageData = resp.data;
        this.bannerUrl =  resp.data.get_banner[0].banner_image;
        this.bannerCaption =  resp.data.get_banner[0].banner_image_caption;
        this.bannerAlt = resp.data.get_banner[0].title;
        this._title.setTitle(resp.data.page_meta_tag_title); 
        this.meta.addTags([
          { name: 'keywords', content: resp.data.page_meta_tag_keywords },
          { name: 'description', content: resp.data.page_meta_tag_description },
          // ...
        ]);             
        //console.log(resp);
      }
    },err => {
      console.log(err);
    });
  }
    getFilterCareerSearch() {
    this._intsearchService.getFilterCareerSearchOptions(this.accessToken, this.jobSector)
    .subscribe((resp) => {
      
      if(resp.status==200)
      {
        this.filterData = resp.data;
        this.jobSectorsMaster = resp.data.job_sectors;
        //console.log(this.filterData);
        this.fliterWageMin = this.filterData.min_wage;
        this.filterWageMax = this.filterData.max_wage;
        this.minWageSlider = this.filterData.min_wage;;
        this.maxWageSlider = this.filterData.max_wage;
        this.initJquery(this.fliterWageMin, this.filterWageMax);
        this.job_sector_sliders();
        this.getCareerData();
       
        
      }      
    }, err => {
      console.log(err);
    });
  }
changeSectorTab()
{
  this._intsearchService.getFilterCareerSearchOptions(this.accessToken, this.jobSector)
  .subscribe((resp) => {
    
    if(resp.status==200)
    {
      this.filterData = resp.data;
      this.fliterWageMin = this.filterData.min_wage;
      this.filterWageMax = this.filterData.max_wage;
      this.minWageSlider = this.filterData.min_wage;;
      this.maxWageSlider = this.filterData.max_wage;
      this.initJquery(this.fliterWageMin, this.filterWageMax);
      this.getCareerData();
      
    }      
  }, err => {
    console.log(err);
  });
}


  initJquery(min_wage:any,max_wage:any)
  {
    let start_wage = 0;
      let end_wage = parseFloat(max_wage)+10;
        $( ()=> {
            $( "#slider-range" ).slider({
                range: true,
                min: min_wage,
                max: max_wage,
                values: [ min_wage, max_wage ],
                slide: ( event:any, ui:any )=> {
                    $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
                  
                      let minwage = ui.values[ 0 ];
                      let maxwage = ui.values[ 1 ];
                      this.minWageSlider = minwage;
                      this.maxWageSlider = maxwage;
                      //console.log(this.minWageSlider);
                      //this.currentPage = 1;
                      this.getCareerData();
                      this.selectedWage = "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ];
                  
                }
            });
          $( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
            " - $" + $( "#slider-range" ).slider( "values", 1 ) );
             
      } );
    
 
    
     

  
  }

  job_sector_sliders()
  {
    setTimeout(() =>{ 
     
      var feat =  $(".mapnavSlide");
      feat.owlCarousel({
        items: 7,
        autoWidth:true,
        loop: false,
        autoplay: false,
        autoplayTimeout: 4900,
        smartSpeed: 800,
        autoplayHoverPause: true,
        lazyLoad: true,
        margin: 5,
        dots: false,
        nav: true,
        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        responsive: {
          0:   { items: 2 },
          480: { items: 3 },
          540: { items: 3 },
          600: { items: 4 },
          768: { items: 5},
          992: { items: 7 },
          1200: { items: 8 },
          1560: { items: 8 }
        },
      });

     }, 1000);
  }
  getCareerData() {
    //console.log(this.userIdNo);
     this._intsearchService.getCareerSearchData(this.accessToken, this.jobLevel, this.jobSector, this.minWageSlider, this.maxWageSlider)
     .subscribe((resp) => {
     
       if(resp.status==200)
       {
         this.careerData = resp.data;
         //console.log(this.careerData);
         
         setTimeout(() =>{ 
          this.makeeffect();
    
         }, 1000);
     
       }      
     }, err => {
       console.log(err);
     });
   }

   makeeffect()
   {
      
      

       this.report('[•] Run Post-DOM Routine...');

			// Init Routine
			this.checkBrowser();
			this.resizeLayout();
			setTimeout(this.resizeLayout, 500);

			// Binding Functions
			$(window).resize(() =>{ this.resizeLayout(); });

      	// Binding Functions: Tooltips
			$(".tooltipped .tooltip-toggle").click((event:any) =>{
				this.report("[t] Tooltip clicked: " +  $(event.currentTarget).parent().attr("data-slug"));
				var obj_tooltip =  $(event.currentTarget).parent().find(".tooltip");
				if (obj_tooltip.is(':visible')){ obj_tooltip.fadeOut(100); }
				else { $(".tooltip").hide(); obj_tooltip.fadeIn(250); }
				return false;
			});
			$(".tooltipped .tooltip .tooltip-box .close").click((event:any) =>{
        $(event.currentTarget).parent().parent().fadeOut(100);
				return false;
			});

      // Binding Functions: Sidebar
			$("#sidebar ul.map-nav > li > a").click((event:any) =>{
				this.report("[☰] Sidenav section click: " +  $(event.currentTarget).parent().attr("class"));
				const obj_parent_li = $(this).parent();
				if (obj_parent_li.hasClass("open")){
					obj_parent_li.removeClass("open");
					obj_parent_li.find(".subnav").slideUp(250);
				} else {
					$("#sidebar ul.map-nav > li").removeClass("open");
					$("#sidebar ul.map-nav > li .subnav").slideUp(250);
					obj_parent_li.addClass("open");
					obj_parent_li.find(".subnav").stop().slideDown(250);
				}
				return false;
			});
			$("#sidebar ul.map-nav > li > ul > li > a").hover((e:any) => {
				$("#map .map-grid .point[data-slug='" +$(e.currentTarget).attr("data-slug") + "'] a.square").trigger(e.type);
			})
      
      // Binding Functions: Route Items
			$("#map .routes a.route-item").click((event:any) =>{
				this.report("[→] Route item clicked: " + $(event.currentTarget).attr("data-slug"));
				this.resetPoints();

				// Tab Action
				if ($(this).parent().hasClass("tab")){ this.openTab($(event.currentTarget).attr("data-slug")); }

				// Subroute Action
				if ($(this).hasClass("subroute")){
					$(this).parent().parent().find(".subroute.active").removeClass("active");
					$(this).addClass("active");
				}

				// Item Action
				switch($(this).attr("data-type")){
					case "careerPath":
						this.report("[→] Plot career path: " + $(event.currentTarget).attr("data-careerpath"));
						$("#map .map-grid").addClass("locked");
						var array_points = $(this).attr("data-careerpath").split("|");
						var array_tooltips = $(this).attr("data-tooltips").split("|");
						for (var i = 0; i < array_points.length; i++) { this.plotPathPoint(array_points[i], array_points[i + 1], array_tooltips[i + 1], (i * 1000)); }
						break;
					
					case "jobList":
						this.report("[→] Plot job list: " + $(event.currentTarget).attr("data-joblist"));
						$("#map .map-grid").addClass("locked");
						var array_points = $(event.currentTarget).attr("data-joblist").split("|");
						for (i = 0; i < array_points.length; i++) { this.plotPoint(array_points[i], (i * 250)); }
						break;
					
					case "freeform":
						$("#map .map-grid").removeClass("locked");
						break;
				}
				return false;
			});
      

      $("#map .map-grid .point a.square").hover(
        (event:any) =>{
          if (!$(this).parent().hasClass("selected") && !$("#map .map-grid").hasClass("locked")){
            this.report("[○] Point hover on: " + $(event.currentTarget).parent().attr("data-slug"));
            this.activatePoint($(event.currentTarget).parent());
          }
        },
        (event:any) =>{
          if (!$(this).parent().hasClass("selected") && !$("#map .map-grid").hasClass("locked")){
            this.report("[○] Point hover off: " + $(event.currentTarget).parent().attr("data-slug"));
            this.deactivatePoint($(event.currentTarget).parent());
          }
        }
      );

      $("#map .map-grid .point a.square").click((event:any) =>{
				if ($("#map .map-grid").hasClass("locked")){
					$("#map .routes a.route-item[data-type='freeform']").click();
					$(event.currentTarget).click();
				} else {
					if ($(event.currentTarget).parent().hasClass("selected")){
						this.report("[○] Point clicked to close: " + $(event.currentTarget).parent().attr("data-slug"));
						this.deselectPoint($(event.currentTarget).parent());
					} else {
						this.report("[○] Point clicked to open: " + $(event.currentTarget).parent().attr("data-slug"));
						if ($("#map .map-grid .point.selected").length){ this.deselectPoint($("#map .map-grid .point.selected")); }
						this.selectPoint($(event.currentTarget).parent());
					}
				}
				return false;
			});
			$("#map .map-grid .point .description .description-box a.close").click((event:any) =>{
				this.report("[○] X clicked to close: " + $(event.currentTarget).parent().parent().parent().attr("data-slug"));
				this.deselectPoint($(event.currentTarget).parent().parent().parent());
				return false;
			});
      $("body").removeClass("preload");

			this.report('[•] Post-DOM Routine complete.');
   }

   report(str:any){
    if(typeof console != 'undefined') {
      str += '';
      if (str.substr(0,3) == '[!]'){ alert(str); }
      else if (str.substr(0,1) == '[')
      { 
        //console.log(str);
       }
      else {
        // console.log('[ ] ' + str); 
      }
    }
  }

  activatePoint(obj_point:any){
    this.report("[○] activatePoint: " + obj_point.attr("data-slug"));
    $("#sidebar .subnav a[data-slug='" + obj_point.attr("data-slug") + "']").addClass("active");
    //$("#map .map-grid .point a.square .dot").addClass("dimmed");
    //$(this).addClass("dimmed");
    obj_point.addClass("dimmed");
    obj_point.addClass("active");
    obj_point.find(".square .dot").width(px_block_width / 4);
    obj_point.find(".square .dot").height(px_block_width / 4);
    obj_point.find(".square .dot").css("top", (px_block_width / 24));
    obj_point.find(".square .dot").css("left", (px_block_width / 24));
    obj_point.find(".square .border").width(px_block_width / 4);
    obj_point.find(".square .border").height(px_block_width / 4);
    obj_point.find(".square .border").css("top", ((px_block_width / 24) - px_dotborder_width));
    obj_point.find(".square .border").css("left", ((px_block_width / 24) - px_dotborder_width));
    // Title Positioning
    if (obj_point.hasClass("x40-installation")){
      obj_point.find(".title").css("left", -(px_block_width * 0.75));
      obj_point.find(".title").css("textAlign", "right");
    } else {
      obj_point.find(".title").css("left", (px_block_width / 3));
    }
    obj_point.find(".title").css("top", ((px_block_width / 6) - (obj_point.find(".title").height() / 2)));
    obj_point.find(".title").stop().fadeIn(500);
  }


  
  deactivatePoint(obj_point:any){
    this.report("[○] deactivatePoint: " + obj_point.attr("data-slug"));
    if (!$("#map .map-grid").hasClass("selected")){ $("#map .map-grid .point a.square .dot").removeClass("dimmed"); }
    $("#sidebar .subnav a[data-slug='" + obj_point.attr("data-slug") + "']").removeClass("active");
    obj_point.removeClass("active");
    obj_point.find(".square .dot").width(px_block_width / 6);
    obj_point.find(".square .dot").height(px_block_width / 6);
    obj_point.find(".square .dot").css("top", (px_block_width / 12));
    obj_point.find(".square .dot").css("left", (px_block_width / 12));
    obj_point.find(".square .border").height(px_block_width / 6);
    obj_point.find(".square .border").width(px_block_width / 6);
    obj_point.find(".square .border").css("left", ((px_block_width / 12) - px_dotborder_width));
    obj_point.find(".square .border").css("top", ((px_block_width / 12) - px_dotborder_width));
    obj_point.find(".title").stop().fadeOut(0);
  }

  deselectPoint(obj_point:any){
    this.report("[○] deselectPoint: " + obj_point.attr("data-slug"));
    $("#map .map-grid").removeClass("selected");
    obj_point.removeClass("selected");
    $("#sidebar .subnav a[data-slug='" + obj_point.attr("data-slug") + "']").removeClass("selected");
    obj_point.find(".description").css("display", "none");
    obj_point.find(".description").css("opacity", 0);
    $("#map .map-grid .line").remove();
    $("#map .map-grid .tooltip").remove();
    $("#map .map-grid .point").removeClass("routed");
    this.deactivatePoint(obj_point);
  }
  selectPoint(obj_point:any){
    this.activatePoint(obj_point);
    $("#map .map-grid").addClass("selected");
    obj_point.addClass("selected");
    $("#sidebar .subnav a[data-slug='" + obj_point.attr("data-slug") + "']").addClass("selected");
    obj_point.find(".title").stop().fadeOut(0);
    obj_point.find(".description").css("display", "block");
    // Description X Positioning
    if (obj_point.hasClass("x10-component")){
      obj_point.find(".description").css("left", 3);
      obj_point.find(".description .description-box").addClass("arrow-left");
    } else if (obj_point.hasClass("x40-installation")){
      obj_point.find(".description").css("left", -((px_block_width * 1.5) - (px_block_width / 4)) + 12);
      obj_point.find(".description .description-box").addClass("arrow-right");
    } else {
      obj_point.find(".description").css("left", -(px_block_width / 2) - 17.5);
      obj_point.find(".description .description-box").addClass("arrow-center");
    }
    // Description Y Positioning
    if (obj_point.hasClass("y90-low-entry")){
      obj_point.find(".description").css("top", -(obj_point.find(".description .description-box").outerHeight()) - 20);
      obj_point.find(".description .description-box").addClass("arrow-bottom");
    } else {
      obj_point.find(".description").css("top", (px_block_width / 3));
      obj_point.find(".description .description-box").addClass("arrow-top");
    }
    obj_point.find(".description").animate({opacity:1}, 100);
    // Line Positioning
    if (obj_point.attr("data-routes") != undefined){
      var array_routes = obj_point.attr("data-routes").split("|");
      var array_tooltips = obj_point.attr("data-tooltips").split("|");
      for (var i = 0; i < array_routes.length; i++) { 
        this.drawLine(obj_point.attr("data-slug"), array_routes[i], array_tooltips[i], (i * 250));
      }
    }
  }

  drawLine(slug_point1:any, slug_point2:any, str_tooltip:any, ms_delay:any){
    this.report("[/] drawLine: " + slug_point1 + "-to-" + slug_point2);
    var obj_point1 = $("#map .map-grid .point[data-slug='" + slug_point1 + "']");
    var y_point1 = parseInt(obj_point1.css("top")) + (px_block_width / 6) - (px_line_width / 2);
    var x_point1 = parseInt(obj_point1.css("left")) + (px_block_width / 6);
    var obj_point2 = $("#map .map-grid .point[data-slug='" + slug_point2 + "']");
    var y_point2 = parseInt(obj_point2.css("top")) + (px_block_width / 6) - (px_line_width / 2);
    var x_point2 = parseInt(obj_point2.css("left")) + (px_block_width / 6);
    var px_length = Math.sqrt((x_point1 - x_point2)*(x_point1 - x_point2) + (y_point1 - y_point2)*(y_point1 - y_point2));
    var deg_angle = Math.atan2(y_point2 - y_point1, x_point2 - x_point1) * 180 / Math.PI;
    
    var obj_new_line = $('<a>').delay(ms_delay + 100).appendTo("#map .map-grid").addClass("line").attr("href","#").attr("data-slug", slug_point1 + "-to-" + slug_point2).css({"display":"block", "top":y_point1, "left":x_point1, "transform":"rotate(" + deg_angle + "deg)"}).animate({width:px_length}, 500);
    obj_new_line.hover(
      (event:any) =>{
        this.report("[/] Line hover on: " + $(event.currentTarget).attr("data-slug"));
        $(event.currentTarget).addClass("active");
        $("#map .map-grid .tooltip[data-slug='" + $(event.currentTarget).attr("data-slug") + "']").stop().fadeIn(250);
      },
      (event:any) =>{
        this.report("[/] Line hover off: " +$(event.currentTarget).attr("data-slug"));
        $(event.currentTarget).removeClass("active");
        $("#map .map-grid .tooltip[data-slug='" + $(event.currentTarget).attr("data-slug") + "']").stop().fadeOut(0);
      }
    );
    obj_new_line.click(function(){
      return false;
    });
    
    var obj_new_tooltip = $('<div>').delay(ms_delay + 600).appendTo("#map .map-grid").addClass("tooltip").attr("data-slug", slug_point1 + "-to-" + slug_point2);
    if (str_tooltip != ""){
      var obj_new_tooltip_box = $('<div>').delay(ms_delay + 600).appendTo(obj_new_tooltip).addClass("tooltip-box").html(str_tooltip);
      var y_midpoint = (( y_point1 + y_point2 ) / 2);
      var x_midpoint = (( x_point1 + x_point2 ) / 2);
      var y_tooltip;
      var x_tooltip;
      obj_new_tooltip.width(px_block_width * 0.75);
      if (deg_angle < -45 && deg_angle > -135){
        y_tooltip = y_midpoint - (obj_new_tooltip.height() / 3);
        obj_new_tooltip.addClass("arrow-side");
      } else {
        y_tooltip = y_midpoint - obj_new_tooltip.height();
        obj_new_tooltip.addClass("arrow-bottom");
      }
      if ((deg_angle >= 0 && deg_angle <= 180) || (deg_angle <= -135 && deg_angle >= -180) || (deg_angle <= 0 && deg_angle >= -45)){
        x_tooltip = x_midpoint - (obj_new_tooltip.width() / 2);
        obj_new_tooltip.addClass("arrow-center");
      } else {
        if (x_midpoint > ($("#map .map-grid").width() / 2)){
          x_tooltip = x_midpoint - obj_new_tooltip.outerWidth() - px_tooltip_arrow_width;
          obj_new_tooltip.addClass("arrow-right");
        } else {
          x_tooltip = x_midpoint + px_tooltip_arrow_width;
          obj_new_tooltip.addClass("arrow-left");
        }
      }
      obj_new_tooltip.css({"top":y_tooltip, "left":x_tooltip})
    }
    obj_new_tooltip.hover(
      (event:any) =>{
        this.report("[/] Line tooltip hover on: " + $(event.currentTarget).attr("data-slug"));
        $("#map .map-grid .line[data-slug='" + $(event.currentTarget).attr("data-slug") + "']").addClass("active");
        $(event.currentTarget).show();
      },
      (event:any) =>{
        this.report("[/] Line tooltip hover off: " + $(event.currentTarget).attr("data-slug"));
        $("#map .map-grid .line[data-slug='" + $(event.currentTarget).attr("data-slug") + "']").removeClass("active");
        $(event.currentTarget).stop().fadeOut(0);
      }
    );
    obj_point1.addClass("routed");
    this.delayAddClass(obj_point2, "routed", (ms_delay + 400));
  }

  delayAddClass(obj:any, str_class:any, ms_delay:any){ array_timeouts[array_timeouts.length] = setTimeout(function(){ obj.addClass(str_class); }, ms_delay); }


  onJoblevelSelected(value:any){
    let opt_text = $('#job_level :selected').data('id');
    if(opt_text!=undefined)
    {
      this.selectedJobLevel = opt_text;
    }
    else
    {
      this.selectedJobLevel = '';
    }
    this.jobLevel = value;
    //this.currentPage = 1;
    //this.getCareerData(this.currentPage);
    this.getCareerData();
  }
  currentSelectedSector(sector_id:any,sector_name:any)
  {
   
    this.jobSectorName = sector_name;
    localStorage.setItem('jbSector', sector_id);
    localStorage.setItem('jbSectorName', sector_name);
    this.jobSector = sector_id;
    this.changeSectorTab();
    
   
  }
  closeSelected(filter:any)
  {
    //console.log(filter);
    if(filter=='jl')
    {
      $('#job_level').val('');
      this.selectedJobLevel='';
      this.currentPage = 1;
      this.jobLevel = '';
      this.getCareerData();
    }
    else if(filter=='wg'){
      this.minWageSlider = this.fliterWageMin;
      this.maxWageSlider = this.filterWageMax;
      this.selectedWage = '';
      this.initJquery(this.minWageSlider, this.maxWageSlider);
      this.currentPage = 1;
      this.getCareerData();
      
    }
  }
  unselectAll()
  {
    $('#job_level').val('');
    this.selectedJobLevel='';
    this.jobLevel = '';
    this.minWageSlider = this.fliterWageMin;
    this.maxWageSlider = this.filterWageMax;
    this.selectedWage = '';
    this.initJquery(this.minWageSlider, this.maxWageSlider);
    this.getCareerData();

  }



   /*  ==========================================================================
		Utility Functions
		========================================================================== */
		checkBrowser(){
			// Mobile
			(function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))bool_mobile = true})(navigator.userAgent||navigator.vendor);
			this.report('[•] checkBrowser(): Mobile == ' + bool_mobile);

			// Touch
			bool_touch = 'ontouchstart' in document.documentElement;
			this.report('[•] checkBrowser(): Touch == ' + bool_touch);

			// IE
			var ua = window.navigator.userAgent;
			var msie = ua.indexOf('MSIE ');
			if (msie > 0){ bool_ie = true; }
			this.report('[•] checkBrowser(): Internet Explorer == ' + bool_ie);
		}

    /*  ==========================================================================
		Binding Functions
		========================================================================== */
		resizeLayout(){
			// Size Elements
			px_block_width = $("#map .map-row .block").width();
			$("#map .map-row .block").height(px_block_width);
			$("#map .map-grid").width(px_block_width * 10);
			$("#map .map-grid").height(px_block_width * 3);
			$("#map .map-grid .point a.square").width(px_block_width / 10);
			$("#map .map-grid .point a.square").height(px_block_width / 10);
			$("#map .map-grid .point a.square").css("marginTop",(px_block_width / 24));
			$("#map .map-grid .point a.square").css("marginLeft",(px_block_width / 24));
			$("#map .map-grid .point a.square .dot").width(px_block_width / 6);
			$("#map .map-grid .point a.square .dot").height(px_block_width / 6);
			$("#map .map-grid .point a.square .dot").css("top", (px_block_width / 12));
			$("#map .map-grid .point a.square .dot").css("left", (px_block_width / 12));
			$("#map .map-grid .point a.square .border").width(px_block_width / 6);
			$("#map .map-grid .point a.square .border").height(px_block_width / 6);
			$("#map .map-grid .point a.square .border").css("top", ((px_block_width / 12) - px_dotborder_width));
			$("#map .map-grid .point a.square .border").css("left", ((px_block_width / 12) - px_dotborder_width));
			$("#map .map-grid .point .title").width(px_block_width * 0.75);
			$("#map .map-grid .point .description").width(px_block_width * 1.5);
			$("#map .map-row .row-label").height(px_block_width);
			$("#map .map-row .col-label .tail.left").css('borderWidth', (px_block_width/4) + 'px ' + (px_block_width/2.04) + 'px 0 0');
			$("#map .map-row .col-label .tail.right").css('borderWidth', '0 ' + (px_block_width/2.04) + 'px ' + (px_block_width/4) + 'px 0');
			$("#map .map-row .axis-label").css('paddingTop', (px_block_width/10));
			$("#map .map-row .row-label .title").css('width', px_block_width);
			$("#map .map-row .row-label .title").css('marginLeft', -(px_block_width));
			$("#map .map-row .row-label .title").css('lineHeight', $("#map .map-row .row-label").width() + 'px');

			// Deactivate Selections
			if ($("#map .map-grid .point.selected").length){ this.deselectPoint($("#map .map-grid .point.selected")); }

			// Industry Video Menu Match
			if ($(".industry .video-menu").length && $(window).width() > 540){
				$(".industry .video-menu a").css("height", $(".industry .video-menu a").width());
				$(".industry .video-menu a .screen").css("height", $(".industry .video-menu a").width());
			}

			this.vertMatch;
		}

    vertMatch(){
			$(".vert-match").each((event:any) =>{
				for (var i = 1; i <= 5; i++){
					if ($(event.currentTarget).find(".vert-match-box-" + i).length){
            $(event.currentTarget).find(".vert-match-box-" + i).css("height", "auto");
						$(event.currentTarget).find(".vert-match-box-" + i + " .vcenter").css("paddingTop", 0);
						var px_height = 0;
						$(event.currentTarget).find(".vert-match-box-" + i).each(() =>{ if ($(event.currentTarget).innerHeight() > px_height){ px_height =$(event.currentTarget).innerHeight(); } });
						$(event.currentTarget).find(".vert-match-box-" + i).css("height", px_height);
						$(event.currentTarget).find(".vert-match-box-" + i).each(() =>{
							if ($(event.currentTarget).find(".vcenter").length){
								const obj_vcenter = $(this).find(".vcenter");
								obj_vcenter.css("paddingTop", ((px_height - obj_vcenter.height()) / 2));
							}
						});
					}
				}
			});
		}
    resetPoints(){
			this.report("[○] resetPoints");
			this.clearArrayTimeouts();
			$("#map .map-grid .point.selected").each(() =>{ this.deselectPoint($(this)); });
			$("#map .map-grid .point.active").each(() =>{ this.deselectPoint($(this)); });
		}
    clearArrayTimeouts(){ for (var i = 0; i < array_timeouts.length; i++){ clearTimeout(array_timeouts[i]); } }
    openTab(str_slug:any){
			this.report("[→] Route tab opened: " + str_slug);
			$("#map .routes ul.tabs li.tab a.active").removeClass("active");
			$("#map .routes .tab-content").slideUp(500);
			$("#map .routes .tab-content[data-slug='" + str_slug + "']").slideDown(500);
			$("#map .routes ul.tabs li.tab a[data-slug='" + str_slug + "']").addClass("active");
			if ($("#map .routes .tab-content[data-slug='" + str_slug + "'] .subroutes").length){
				$("#map .routes .tab-content[data-slug='" + str_slug + "'] .subroutes .route-item:first").click();
			}
		}

    /*  ==========================================================================
		Map Functions
		========================================================================== */
		plotPoint(slug_point:any, ms_delay:any){
		this.report("[○] plotPoint: " + slug_point);
			array_timeouts[array_timeouts.length] = setTimeout(() =>{ this.activatePoint($("#map .map-grid .point[data-slug='" + slug_point + "']")); }, ms_delay);
		}
    plotPathPoint(slug_point1:any, slug_point2:any, str_tooltip:any, ms_delay:any){
			this.report("[○] plotPathPoint: " + slug_point1 + " to " + slug_point2);
			array_timeouts[array_timeouts.length] = setTimeout(() =>{
				this.activatePoint($("#map .map-grid .point[data-slug='" + slug_point1 + "']"));
				this.drawLine(slug_point1, slug_point2, str_tooltip, 500);
			}, ms_delay);
		}

    checkLogeedIn() {
      this._intsearchService.checkLoginState(this.apiKey, this.accessToken)
      .subscribe(res => {
        if(res.status != 200) { 
          localStorage.removeItem('access_token');
          localStorage.removeItem('user_name');
          localStorage.removeItem('user_role');
          localStorage.removeItem('user_id'); 
        }
        else
        {
          
          this.isLoggedInchk = true;
          let userName = localStorage.getItem('user_name');
          let userRole = localStorage.getItem('user_role');
          if(!this.isLoggedInchk)
          {
            this._afterLoginService.setLoggedInNameSubject('Register');
            this._afterLoginService.setLoggedOutNameSubject('Sign In'); 
            
          }else{
            if(userName!='' && userName!=undefined)
            {
              this._afterLoginService.setLoggedInNameSubject(userName); 
              this._afterLoginService.setLoggedOutNameSubject('Log Out');
              this.userRole = userRole; 
            }
            if(this.userRole == 1)
            {
              this._afterLoginService.setLoggedInUserRoleSubject('student');
            }  
            if(this.userRole == 2)
            {
              this._afterLoginService.setLoggedInUserRoleSubject('professional');
            }  
            

          }
        }
      },err => {
        console.log(err);
      });
      
    }

}
