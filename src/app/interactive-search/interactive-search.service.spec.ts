import { TestBed } from '@angular/core/testing';

import { InteractiveSearchService } from './interactive-search.service';

describe('InteractiveSearchService', () => {
  let service: InteractiveSearchService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(InteractiveSearchService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
