import { HttpHeaders, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { CanActivate, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment';
import { Observable, of as ObservableOf, from, Subject } from 'rxjs';
import { catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public accessToken: string = '';
  public getLoggedInName = new Subject();

  constructor(private _http:HttpClient, private _router:Router, private _toastr:ToastrService) { }
  canActivate(): Observable<boolean> | Promise<boolean> | boolean {
    try {
      let accessToken = localStorage.getItem(btoa('access_token'));

      if(typeof accessToken !== 'undefined' && accessToken != null && accessToken != '') {
        this.accessToken = atob(accessToken);
        let fd = new FormData();

        let headers = new HttpHeaders()
        .set('Authorization', 'Bearer ' + this.accessToken);

        return this._http.post(environment.apiBaseUrl + 'auth/is-logged-in', fd, { headers: headers })
        .pipe(map(res => {
          let resp = JSON.parse(JSON.stringify(res));
          
          if(resp.status == 200) {
            this._toastr.success('Login successfully');
            this.getLoggedInName.next(resp.user.name);
            if(resp.user.role_id==1){this._router.navigateByUrl('/student/my-profile');}
            if(resp.user.role_id==2){this._router.navigateByUrl('/professional/my-profile');}
            
            return false;
          }
          else { 
            this.getLoggedInName.next('Register');                      
            return true;
          }
        }, () => {
          this.getLoggedInName.next('Register');
          return true;
        }))
        .pipe(catchError((err: HttpErrorResponse): Observable<boolean> => {          
          return ObservableOf(true);
        }));
      }else{
        return true;
      }
    } catch (error) {      
      return true;
    }
  }

  canActivateChild(): Observable<boolean> | Promise<boolean> | boolean {
    try {
      let accessToken = localStorage.getItem(btoa('access_token'));

      if(typeof accessToken !== 'undefined' && accessToken != null && accessToken != '') {
        this.accessToken = atob(accessToken);
        let fd = new FormData();

        let headers = new HttpHeaders()
        .set('Authorization', 'Bearer ' + this.accessToken);

        return this._http.post(environment.apiBaseUrl + 'auth/is_logged_in', fd, { headers: headers })
        .pipe(map(res => {
          let resp = JSON.parse(JSON.stringify(res));
          if(resp.status == 200) {
            return true;
          }
          else {  
            this._router.navigateByUrl('/login');     
            return false;
          }
        }, () => {
          this._router.navigateByUrl('/login');     
          return false;
        }))
        .pipe(catchError((err: HttpErrorResponse): Observable<boolean> => { 
          this._router.navigateByUrl('/login');          
          return ObservableOf(false);
        }));
      }
      else{
        this._router.navigateByUrl('/login');     
        return false;
      }
    } catch (error) { 
      this._router.navigateByUrl('/login');     
      return false;
    }
  }
}
