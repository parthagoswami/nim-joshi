import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthService as AuthGaurd } from './auth.service';
import { AuthStudentService as AuthSudentGuard } from './auth-student.service'
import { AuthProfessionalService as AuthProfessionalGuard } from './auth-professional.service'
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { MyProfileComponent } from './after-login/my-profile/my-profile.component';
import { AfterLoginComponent } from './after-login/after-login.component';
import { GuideMeComponent } from './after-login/guide-me/guide-me.component';
import { ExploreCareerComponent } from './after-login/explore-career/explore-career.component';
import { CareerSearchComponent } from './after-login/career-search/career-search.component';
import { CareerGoalComponent } from './after-login/career-goal/career-goal.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { HowItWorksComponent } from './how-it-works/how-it-works.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { HomeSearchComponent } from './home-search/home-search.component';
import { CareerDetailsComponent } from './career-details/career-details.component';
import { InteractiveSearchComponent } from './interactive-search/interactive-search.component';
import { TermsConditionsComponent } from './terms-conditions/terms-conditions.component';
import { FaqsComponent } from './faqs/faqs.component';
import { ChangePasswordComponent } from './after-login/change-password/change-password.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/index' },
  { path: 'index', component: HomeComponent },
  { path: 'about-us', component: AboutUsComponent },
  { path: 'how-it-works', component: HowItWorksComponent },
  { path: 'contact-us', component: ContactUsComponent },
  { path: 'terms-and-conditions', component: TermsConditionsComponent },
  { path: 'faq', component: FaqsComponent },
  { path: 'search', component: HomeSearchComponent },
  { path: 'interactive-search', component: InteractiveSearchComponent },
  { path: 'career/:slug', component: CareerDetailsComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'forgot-password', component: ForgotPasswordComponent },
  { path: 'reset-password/:token/:email', component: ResetPasswordComponent },
  { path: 'login', component: LoginComponent, canActivate: [AuthGaurd] },
  {path: 'student', component: LoginComponent, canActivate: [AuthGaurd]},
  {path: 'professional', component: LoginComponent, canActivate: [AuthGaurd]}, 
  {path: 'my-profile', component: LoginComponent, canActivate: [AuthGaurd]},
  {
    path: 'student',
    component: AfterLoginComponent,
    canActivateChild: [AuthSudentGuard],
    children: [{
      path: 'my-profile',
      component: MyProfileComponent
    },
    {
      path: 'change-password',
      component: ChangePasswordComponent
    },
    {
      path: 'guide-me',
      component: GuideMeComponent
    },
    {
      path: 'explore-career',
      component: ExploreCareerComponent
    },
    {
      path: 'career-search',
      component: CareerSearchComponent
    },
    {
      path: 'career-goal',
      component: CareerGoalComponent
    }
  ]
  },
  {
    path: 'professional',
    component: AfterLoginComponent,
    canActivateChild: [AuthProfessionalGuard],
    children: [{
      path: 'my-profile',
      component: MyProfileComponent
    },
    {
      path: 'change-password',
      component: ChangePasswordComponent
    },
    {
      path: 'guide-me',
      component: GuideMeComponent
    },
    {
      path: 'explore-career',
      component: ExploreCareerComponent
    },
    {
      path: 'career-search',
      component: CareerSearchComponent
    },
    {
      path: 'career-goal',
      component: CareerGoalComponent
    }
  ]
  },
  
  { path: '**',  component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
