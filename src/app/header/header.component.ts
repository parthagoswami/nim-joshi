import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HeaderService } from './header.service';
import { environment } from '../../environments/environment';
import { AfterLoginService } from '../after-login/after-login.service';
declare var $:any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  public accessToken: string = '';
  public settingData: any = [];
  public apiFileUrl: string = environment.apiFileUrl;
  public loggedInName: string = 'Register';
  public loggedOutName: string = 'Sign In';
  public apiKey: string = environment.apiKey; 
  public profilePath: string = '';
  public searchKey: string = '';
  public currentSerachKey: string = '';
  public logoFileUrl: string = '';
  public isSearchkeyempty: boolean = true;
  public isSearchclicked: boolean = false;
  

  constructor(private _headerService: HeaderService, private _router: Router,private _afterLoginService:AfterLoginService,private _cdr:ChangeDetectorRef) { }

  ngOnInit(): void {
    let accessToken = localStorage.getItem(btoa('access_token'));
    
    if(typeof accessToken !== 'undefined' && accessToken != null && accessToken != '') {
      this.accessToken = atob(accessToken); 
      }

    this._afterLoginService.getLoggedInNameSubject().subscribe(resp => {
      this.loggedInName = resp;
      this._cdr.detectChanges();
      
    });
    this._afterLoginService.getLoggedInUserRoleSubject().subscribe(resp => {
      this.profilePath = '/'+resp+'/my-profile';
      this._cdr.detectChanges();
     
    });

    this._afterLoginService.getLoggedOutNameSubject().subscribe(resp => {
      this.loggedOutName = resp;
      this._cdr.detectChanges();
      
    });
    
    
    try{
    
      
      this.getSettingData();
    }catch(error){
      console.log(error);
    }
    let searchKeyval = localStorage.getItem('searchKey');
     
    if(typeof searchKeyval !== 'undefined' && searchKeyval != null && searchKeyval != '')
    {
      this.currentSerachKey = searchKeyval;
    }
    this.initJquery();
    setTimeout(() =>{ $(".lazy").lazy(); }, 1000);
  }

  getSettingData(){ 
    this.logoFileUrl = '';  
    this._headerService.getCommonSettingData()
    .subscribe((resp)=> {      
      if(resp.status == 200) {
        this.settingData = resp.data;  
        this.logoFileUrl =this.apiFileUrl+this.settingData.header_logo;       
        
      }
    },err => {
      console.log(err);
    });
  }

  logoutClicked() {
   
    this._afterLoginService.logOut(this.apiKey, this.accessToken)
    .subscribe(res => {
      
      localStorage.removeItem(btoa('access_token'));
      localStorage.removeItem('user_name');
      localStorage.removeItem('user_role'); 
      localStorage.removeItem('user_id');
      window.location.reload();
    },err => {
      //console.log(err);
      localStorage.removeItem(btoa('access_token'));
      localStorage.removeItem('user_name');
      localStorage.removeItem('user_role'); 
      localStorage.removeItem('user_id');
      window.location.reload();
    });    
    
  }

  initJquery()
  {
    var $searchToggle = $(".searchToggle");
        var $searchClose = $("#searchClose");
        var $searchContainer = $("#searchContainer");

        $searchToggle.click(function() {
            $searchContainer.slideDown(300);
        });
        $searchClose.click(function() {
            $searchContainer.slideUp(300);
        });
  }
  
  
  searchKeyword(): void {
    this.isSearchkeyempty=true;
    this.isSearchclicked = true;
   
    if(this.searchKey!='' )
    {
      this.isSearchkeyempty=false;
      localStorage.setItem('searchKey', this.searchKey);
      this._router.navigateByUrl('/search');
    }
    else
    {
      this.isSearchkeyempty=true;
    }
  
  }



}
