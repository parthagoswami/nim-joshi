import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HeaderService {

  constructor(private _http: HttpClient) { }
  getCommonSettingData(){
    let fd = new FormData();
    // let headers = new HttpHeaders()
    // .set('Authorization', 'Bearer ' + accessToken);

    return this._http.get(environment.apiBaseUrl + 'common-settings')
    .pipe(map(res => JSON.parse(JSON.stringify(res))));
  }

  

}
