import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AfterLoginService } from '../after-login/after-login.service';
import { FaqsService } from './faqs.service';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-faqs',
  templateUrl: './faqs.component.html',
  styleUrls: ['./faqs.component.css']
})
export class FaqsComponent implements OnInit {

  public pageData: any = [];
  public bannerUrl: any = [];
  public bannerCaption: any = [];
  public bannerAlt: string = '';
  public apiFileUrl: string = environment.apiFileUrl;
  public accessToken: string = '';
  public userRole: any;
  public apiKey: string = environment.apiKey;
  public isLoggedInchk : boolean = false;

  constructor(private _faqService:FaqsService,private _router:Router,private _title:Title,private _toastr:ToastrService,private meta:Meta,private _afterLoginService:AfterLoginService) {
    let accessToken = localStorage.getItem(btoa('access_token'));
    if(typeof accessToken !== 'undefined' && accessToken != null && accessToken != '') {
      this.accessToken = atob(accessToken);
    }
    this.checkLogeedIn();
   }

  ngOnInit(): void {
    try{
      this.getfaqSettingData();
    }catch(error){
      console.log(error);
    }
  }
  getfaqSettingData(){    
    this._faqService.getFaqSettingData()
    .subscribe((resp)=> {      
      if(resp.status == 200) {
        this.pageData = resp.data;
        this.bannerUrl =  resp.data.get_banner[0].banner_image;
        this.bannerCaption =  resp.data.get_banner[0].banner_image_caption;
        this.bannerAlt = resp.data.get_banner[0].title;
        this._title.setTitle(resp.data.page_meta_tag_title); 
        this.meta.addTags([
          { name: 'keywords', content: resp.data.page_meta_tag_keywords },
          { name: 'description', content: resp.data.page_meta_tag_description },
          // ...
        ]);                  
        
      }
    },err => {
      console.log(err);
    });
  }
  checkLogeedIn() {
    this._faqService.checkLoginState(this.apiKey, this.accessToken)
    .subscribe(res => {
      if(res.status != 200) { 
        localStorage.removeItem('access_token');
        localStorage.removeItem('user_name');
        localStorage.removeItem('user_role');
        localStorage.removeItem('user_id'); 
      }
      else
      {
        
        this.isLoggedInchk = true;
        let userName = localStorage.getItem('user_name');
        let userRole = localStorage.getItem('user_role');
        if(!this.isLoggedInchk)
        {
          this._afterLoginService.setLoggedInNameSubject('Register');
          this._afterLoginService.setLoggedOutNameSubject('Sign In'); 
        }else{
          if(userName!='' && userName!=undefined)
          {
            this._afterLoginService.setLoggedInNameSubject(userName); 
            this._afterLoginService.setLoggedOutNameSubject('Log Out');
            this.userRole = userRole; 
          }
          if(this.userRole == 1)
          {
            this._afterLoginService.setLoggedInUserRoleSubject('student');
          }  
          if(this.userRole == 2)
          {
            this._afterLoginService.setLoggedInUserRoleSubject('professional');
          }  
          

        }
      }
    },err => {
      console.log(err);
    });
    
  }

}
