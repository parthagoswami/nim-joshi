import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ForgotPasswordService {

  constructor(private _http:HttpClient) { }
  getForgotPwdSettingData(){
    let fd = new FormData();
    // let headers = new HttpHeaders()
    // .set('Authorization', 'Bearer ' + accessToken);

    return this._http.get(environment.apiBaseUrl + 'cms/forgot-password')
    .pipe(map(res => JSON.parse(JSON.stringify(res))));
  }
  forgotFormSubmit(apiKey: string,formGroupData: any) {
    let fd = new FormData();
    fd.append("email", formGroupData.email);    
   
    let headers = new HttpHeaders()
    .set('X-Auth-Token', apiKey);
    return this._http.post(environment.apiBaseUrl + 'auth/forgot-password', fd, { headers })
    .pipe(map(res => JSON.parse(JSON.stringify(res))));
  }
}
