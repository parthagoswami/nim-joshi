import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Title, Meta } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ForgotPasswordService } from './forgot-password.service';
import { environment } from '../../environments/environment';
declare var $:any;

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  public pageData: any = [];
  public apiFileUrl: string = environment.apiFileUrl;
  public bannerUrl: any = [];
  public bannerCaption: any = [];
  public bannerAlt: string = '';
  public isForgotClicked: boolean = false;
  public apiKey: string = environment.apiKey;
  public successMsg: string = '';
  public errorMsg: string = '';
  public forgotpwdForm: FormGroup = this.formBuilder.group({
    'email': ['', [Validators.required, Validators.pattern(/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i)]],
    
  })
 

  constructor(private _forgotService:ForgotPasswordService,private _title:Title,private formBuilder:FormBuilder,private _router:Router,private _toastr:ToastrService,private meta: Meta) { }

  ngOnInit(): void {
    try{
      this.getForgotPwdSettingData();
    }catch(error){
      //console.log(error);
    }
    
    setTimeout(() =>{ $(".lazy").lazy(); }, 1000);
  }

  getForgotPwdSettingData(){    
    this._forgotService.getForgotPwdSettingData()
    .subscribe((resp)=> {      
      if(resp.status == 200) {
        this.pageData = resp.data;
        this.bannerUrl =  resp.data.get_banner[0].banner_image;
        this.bannerCaption =  resp.data.get_banner[0].banner_image_caption;
        this.bannerAlt = resp.data.get_banner[0].title;
        this._title.setTitle(resp.data.page_meta_tag_title); 
        this.meta.addTags([
          { name: 'keywords', content: resp.data.page_meta_tag_keywords },
          { name: 'description', content: resp.data.page_meta_tag_description },
          // ...
        ]);                  
       
      }
    },err => {
      //console.log(err);
    });
  }
  public ForgotFormSubmitted(): void {
    this.isForgotClicked = true;
    if(this.forgotpwdForm.invalid) {
      return;
    }
    $("#forgotpwdreqSubmit").attr('disabled', true);
    $('#forgotpwdreqSubmit').html('<i class="fa fa-spinner icon-spin"></i> Submit');
    this._forgotService.forgotFormSubmit(this.apiKey, this.forgotpwdForm.value)
    .subscribe(resp => {
      $("#forgotpwdreqSubmit").removeAttr('disabled');
      $('#forgotpwdreqSubmit').html('Submit');
      
      if(resp.status == 200) {
        this._toastr.success(resp.response); 
        this.successMsg =  resp.response;
        $('#success_msg').css('display','block'); 
        $('#error_msg').css('display','none');         
      }
      else if(resp.status == 201) {       
        this._toastr.error(resp.response); 
        this.errorMsg = resp.response;
        $('#success_msg').css('display','none'); 
        $('#error_msg').css('display','block'); 
      } else {       
        this._toastr.error('Something went wrong'); 
        this.errorMsg = 'Something went wrong';
        $('#success_msg').css('display','none'); 
        $('#error_msg').css('display','block'); 
      }
      setTimeout(function(){
        $('#success_msg').css('display','none'); 
        $('#error_msg').css('display','none'); 
     }, 4000); //Time before execution

    }, err => {
      
      //this.loginMsg = err.message;
      this._toastr.error('Something went wrong.');
    });
    
  }

}
