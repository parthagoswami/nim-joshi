import { Component, OnInit } from '@angular/core';
import { ResetPasswordService } from './reset-password.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Title, Meta } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { environment } from '../../environments/environment';
declare var $:any;

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
  public pageData: any = [];
  public apiFileUrl: string = environment.apiFileUrl;
  public bannerUrl: any = [];
  public bannerCaption: any = [];
  public bannerAlt: string = '';
  public isresetClicked: boolean = false;
  public apiKey: string = environment.apiKey;
  public resetToken :string = '';
  public resetEmail:string = '';
  public confirmPwdStat: number = 0;
  public confirmPwdMsg: string = '';
  public successMsg: string = '';
  public errorMsg: string = '';
  public resetPwdForm: FormGroup = this.formBuilder.group({
    'email': ['', [Validators.required, Validators.pattern(/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i)]],
    'password': ['', [Validators.required, Validators.pattern(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$/)]],
    'confirm_password': ['', [Validators.required, Validators.pattern(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$/)]],
    
  })

  constructor(private _resetpwdService:ResetPasswordService,private _title:Title,private formBuilder:FormBuilder,private _router:Router,private _toastr:ToastrService,private _activatedRoute: ActivatedRoute,private meta: Meta) { }

  ngOnInit(): void {
    try{
      this.getResetPwdSettingData();
      
    }catch(error){
      console.log(error);
    }
    setTimeout(() =>{ $(".lazy").lazy(); }, 1000);
    $('#hideshowpwdconf').on('click', function(){
      let current_tp = $('#confirm_password').attr('type');
      if(current_tp=='text')
      {
        $('#confirm_password').attr('type','password');
        $('#hideshowpwdconf').html('<i class="fa fa-eye-slash" aria-hidden="true"></i>');
      }
      else
      {
        $('#confirm_password').attr('type','text');
        $('#hideshowpwdconf').html('<i class="fa fa-eye" aria-hidden="true"></i>');
      }
      
    });
    $('#hideshowpwd').on('click', function(){
      let current_tp = $('#password').attr('type');
      if(current_tp=='text')
      {
        $('#password').attr('type','password');
        $('#hideshowpwd').html('<i class="fa fa-eye-slash" aria-hidden="true"></i>');
      }
      else
      {
        $('#password').attr('type','text');
        $('#hideshowpwd').html('<i class="fa fa-eye" aria-hidden="true"></i>');
      }
      
    });
  }

  getResetPwdSettingData(){    
    this._resetpwdService.getResetPwdSettingData()
    .subscribe((resp)=> {      
      if(resp.status == 200) {
        this.pageData = resp.data;
        this.bannerUrl =  resp.data.get_banner[0].banner_image;
        this.bannerCaption =  resp.data.get_banner[0].banner_image_caption;
        this.bannerAlt = resp.data.get_banner[0].title;
        this.resetToken = this._activatedRoute.snapshot.params.token;
        this.resetEmail = this._activatedRoute.snapshot.params.email;
       
        this._title.setTitle(resp.data.page_meta_tag_title);
        this.meta.addTags([
          { name: 'keywords', content: resp.data.page_meta_tag_keywords },
          { name: 'description', content: resp.data.page_meta_tag_description },
          // ...
        ]);    
        this.resetPwdForm.patchValue({
          email:this.resetEmail,
          
        });  

        console.log(resp);
        console.log('testlog');
      }
    },err => {
      console.log(err);
    });
  }
  public resetPwdFormSubmitted(): void {
    this.isresetClicked = true;
    if(this.resetPwdForm.invalid) {
      return;
    }
    if(this.resetPwdForm.value.password!=this.resetPwdForm.value.confirm_password)
    {
      this.confirmPwdStat=1;
      this.confirmPwdMsg='Confirm Password must be same as Password.';
      this._toastr.error(this.confirmPwdMsg);
      return;
    }
    else
    {
      $("#resetpwdreqSubmit").attr('disabled', true);
      $('#resetpwdreqSubmit').html('<i class="fa fa-spinner icon-spin"></i> Submit');
      this.confirmPwdStat=0;
      this.confirmPwdMsg='';
      this._resetpwdService.resetFormSubmit(this.apiKey, this.resetPwdForm.value,this.resetToken)
      .subscribe(resp => {
        $("#resetpwdreqSubmit").removeAttr('disabled');
        $('#resetpwdreqSubmit').html('Submit');
        console.log(resp);
        if(resp.status == 200) {
          this._toastr.success(resp.response); 
          this.successMsg =  resp.response;
          $('#success_msg').css('display','block'); 
          $('#error_msg').css('display','none');   
          this._router.navigate(['/index']);      
        } else {       
          this._toastr.error('Something went wrong.'); 
          this.errorMsg = 'Something went wrong.';
          $('#success_msg').css('display','none'); 
          $('#error_msg').css('display','block'); 
        }
        setTimeout(function(){
          $('#success_msg').css('display','none'); 
          $('#error_msg').css('display','none'); 
      }, 4000); //Time before execution

      }, err => {
        
        //this.loginMsg = err.message;
        this._toastr.error('Something went wrong.');
      });
      
    }
    
  }

}
