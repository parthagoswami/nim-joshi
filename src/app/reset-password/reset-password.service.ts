import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class ResetPasswordService {

  constructor(private _http:HttpClient) { }

  getResetPwdSettingData(){
    let fd = new FormData();
    // let headers = new HttpHeaders()
    // .set('Authorization', 'Bearer ' + accessToken);

    return this._http.get(environment.apiBaseUrl + 'cms/reset-password')
    .pipe(map(res => JSON.parse(JSON.stringify(res))));
  }
  resetFormSubmit(apiKey: string,formGroupData: any, resetToken:any) {
    let fd = new FormData();
    fd.append("email", formGroupData.email);    
    fd.append("password", formGroupData.password); 
    fd.append("token", resetToken); 
    let headers = new HttpHeaders()
    .set('X-Auth-Token', apiKey);
    return this._http.post(environment.apiBaseUrl + 'auth/reset-password', fd, { headers })
    .pipe(map(res => JSON.parse(JSON.stringify(res))));
  }
}
