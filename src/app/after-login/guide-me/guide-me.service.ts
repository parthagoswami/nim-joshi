import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GuideMeService {

  constructor(private _http:HttpClient) { }
  getGuideMeData(accessToken: string) {
    let fd = new FormData();

    // fd.append("password", formGroupData.password);

    let headers = new HttpHeaders()
    .set('Authorization', 'Bearer ' + accessToken);

    return this._http.post(environment.apiBaseUrl + 'user/assessments-list', fd, { headers })
    .pipe(map(res => JSON.parse(JSON.stringify(res))));
  }
}
