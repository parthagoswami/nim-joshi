import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AfterLoginService } from '../after-login.service';
import { GuideMeService } from './guide-me.service';
import { environment as env } from '../../../environments/environment';
declare var $:any;

@Component({
  selector: 'app-guide-me',
  templateUrl: './guide-me.component.html',
  styleUrls: ['./guide-me.component.css']
})
export class GuideMeComponent implements OnInit {
  public accessToken: string = '';
  public pageData: any = [];

  constructor(private _service: GuideMeService,private _router: Router,private _afterLoginService:AfterLoginService,private _title:Title,private formBuilder:FormBuilder,private _toastr:ToastrService) { }

  ngOnInit(): void {
    try {
    let accessToken = localStorage.getItem(btoa('access_token'));

    if(typeof accessToken !== 'undefined' && accessToken != null && accessToken != '') {
      this.accessToken = atob(accessToken);
      this.getAssessmentData();
    }
    else
      this._router.navigateByUrl('/login');
  } catch (error) {
    this._router.navigateByUrl('/login');
  }

    
    setTimeout(() =>{ $(".lazy").lazy(); }, 1000);
    
  }
  getAssessmentData() {
    this._service.getGuideMeData(this.accessToken)
    .subscribe((resp) => {
    //console.log(resp);
    if(resp.status==200)
    {
      this.pageData = resp.data;
      
      


    
    }      
  }, err => {
    console.log(err);
  });
  }

}


