import { TestBed } from '@angular/core/testing';

import { GuideMeService } from './guide-me.service';

describe('GuideMeService', () => {
  let service: GuideMeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GuideMeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
