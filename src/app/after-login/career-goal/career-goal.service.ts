import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CareerGoalService {

  constructor(private _http: HttpClient) { }
  getUserGoalData(accessToken: string) {
    let fd = new FormData();
    let headers = new HttpHeaders()
    .set('Authorization', 'Bearer ' + accessToken);

    return this._http.post(environment.apiBaseUrl + 'user/career-goals-data', fd, { headers })
    .pipe(map(res => JSON.parse(JSON.stringify(res))));
  }
  getFavCareer(accessToken: string) {
    let fd = new FormData();
    let headers = new HttpHeaders()
    .set('Authorization', 'Bearer ' + accessToken);

    return this._http.post(environment.apiBaseUrl + 'user/fav-careers', fd, { headers })
    .pipe(map(res => JSON.parse(JSON.stringify(res))));
  }
  careerGoalSubmit(apiKey: string, formGroupDataGoal: any,accessToken: string) {
    console.log(formGroupDataGoal);
    
    let headers = new HttpHeaders()
    .set('Authorization', 'Bearer ' + accessToken);  

    return this._http.post(environment.apiBaseUrl + 'user/update_career_goal', formGroupDataGoal, { headers })
    .pipe(map(res => JSON.parse(JSON.stringify(res))));
  }
}
