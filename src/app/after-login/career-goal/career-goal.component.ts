import { Component, OnInit, NgZone } from '@angular/core';
import { FormBuilder, FormArray, FormGroup, Validators, FormControl } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AfterLoginService } from '../after-login.service';
import { CareerGoalService } from './career-goal.service';
import { environment as env } from '../../../environments/environment';
declare var $:any;

@Component({
  selector: 'app-career-goal',
  templateUrl: './career-goal.component.html',
  styleUrls: ['./career-goal.component.css']
})
export class CareerGoalComponent implements OnInit {
  public accessToken: string = '';
  public pageData: any = [];
  public careerData: any = [];
  public apiKey: string = env.apiKey; 
  public goalForm: FormGroup;
  public responeseGoal: string = ''; 
  public isGoalSaveClicked: boolean = false;
  public goalCount: number = 0;
  constructor(private _goalService:CareerGoalService,private _router:Router,private _afterLoginService:AfterLoginService,private _title:Title, private formBuilder:FormBuilder,private _toastr:ToastrService) { 
    this.goalForm = this.formBuilder.group({  
      goals: this.formBuilder.array([]) 
    });  
  }

  ngOnInit(): void {
    try {
      let accessToken = localStorage.getItem(btoa('access_token'));

      if(typeof accessToken !== 'undefined' && accessToken != null && accessToken != '') {
        this.accessToken = atob(accessToken);
        this.getFavcareerData();
        this.getGoalData();
      }
      else
        this._router.navigateByUrl('/login');
    } catch (error) {
      this._router.navigateByUrl('/login');
    }

    this._title.setTitle(`${env.profileTitle}`);

   
  setTimeout(() =>{  
    for( var i=0;i<this.pageData.length;i++)
    {
      
      let selected_status = 'Pending';
      let selected_class = 'pending-btn';
      if(this.pageData[i].goal_status==1)
      {
        selected_status = 'Pending';
        selected_class = 'pending-btn';
        $('#status'+i).removeClass('failed-btn');
        $('#status'+i).removeClass('achieved-btn');

      }
      else if(this.pageData[i].goal_status==2)
      {
        selected_status = 'Achieved';
        selected_class = 'achieved-btn';
        $('#status'+i).removeClass('failed-btn');
        $('#status'+i).removeClass('pending-btn');
      }
      else
      {
        selected_status = 'Failed';
        selected_class = 'failed-btn';
        $('#status'+i).removeClass('achieved-btn');
        $('#status'+i).removeClass('pending-btn');
      }
      $('#status'+i).html(selected_status);
      $('#status'+i).addClass(selected_class);
      
      
    }
  }, 1000);

    setTimeout(() =>{ $(".lazy").lazy(); }, 1000);
  }
  getFavcareerData() {
    this._goalService.getFavCareer(this.accessToken)
    .subscribe((resp) => {
     
      if(resp.status==200)
      {
        this.careerData = resp.data;
        
        
      }
    }, err => {
      console.log(err);
    });
  }
  getGoalData() {
    this._goalService.getUserGoalData(this.accessToken)
    .subscribe((resp) => {
      
      if(resp.status==200)
      {
        this.pageData = resp.data;
        this.responeseGoal = resp.response;
        
      
        for( var i=0;i<this.pageData.length;i++)
        {
          let element_goal:HTMLElement = document.getElementById('auto_trigger_goal') as HTMLElement;
          if(element_goal){
            element_goal.click();
            }
        }
        if(this.pageData.length>0)
        {
          this.goals().setValue(this.pageData);
         
          
        }
        
      }
    }, err => {
      console.log(err);
    });
  }

  goals() : FormArray {  
    return this.goalForm.get("goals") as FormArray  
  }  
  newGoal(): FormGroup {  
    return this.formBuilder.group({   
      'goal_career': ['', [Validators.required]],
      'goal_title': ['', [Validators.required]],  
      'goal_date': ['', [Validators.required]],
      'goal_status': ['', [Validators.required]],
      'cg_tid': '',
    })  
  } 
  
  addGoal() {  
    this.goals().push(this.newGoal());  
    this.goalCount = this.goalCount+1;
  }  
  removeGoal(j:number) {  
    this.goals().removeAt(j);  
    this.goalCount = this.goalCount-1;
  } 
  public careerGoalSubmit(): void {
    
    this.isGoalSaveClicked = true;
    
    if(this.goalForm.invalid) {
        return;
    }
    $('#goalsubmitbtn').attr('disabled','true');
    $('#goalsubmitbtn').html('<i class="fa fa-spinner icon-spin"></i> Update');
    this._goalService.careerGoalSubmit(this.apiKey, this.goalForm.value, this.accessToken)
    .subscribe(resp => {      
      $('#goalsubmitbtn').removeAttr('disabled');
      $('#goalsubmitbtn').html('Update');
   
    if(resp.status == 200) { 
      if(resp.errors!=undefined)
      {
        let errMsg = '';
        $.each(resp.errors, function(name:any, value:any){   
          if(errMsg=='')
          {
            errMsg = value; 
          }
          else
          {
            errMsg = ', '+value; 
          }
                
          
        })
        this._toastr.error(errMsg); 
      } 
      else
      {
        
        this._toastr.success('Goals Updated Successfully.');
        this.goals().setValue(resp.data);
        this.pageData = resp.data;
        setTimeout(() =>{  
          for( var i=0;i<this.pageData.length;i++)
          {
            
            let selected_status = 'Pending';
            let selected_class = 'pending-btn';
            if(this.pageData[i].goal_status==1)
            {
              selected_status = 'Pending';
              selected_class = 'pending-btn';
              $('#status'+i).removeClass('failed-btn');
              $('#status'+i).removeClass('achieved-btn');
      
            }
            else if(this.pageData[i].goal_status==2)
            {
              selected_status = 'Achieved';
              selected_class = 'achieved-btn';
              $('#status'+i).removeClass('failed-btn');
              $('#status'+i).removeClass('pending-btn');
            }
            else
            {
              selected_status = 'Failed';
              selected_class = 'failed-btn';
              $('#status'+i).removeClass('achieved-btn');
              $('#status'+i).removeClass('pending-btn');
            }
            $('#status'+i).html(selected_status);
            $('#status'+i).addClass(selected_class);
            
            
          }
        }, 1000);
        
      }   
      
     
    }
  }, err => {   
    this._toastr.error(err.message);
    });
  }
  currentStatus(value:any){
    //$(value.currentTarget)
    //console.log(value);
  }

}
