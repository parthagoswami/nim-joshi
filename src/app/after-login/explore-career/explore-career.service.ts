import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ExploreCareerService {

  constructor(private _http:HttpClient) { }

  getFilterMaster(accessToken: string) {
    let fd = new FormData();
    let headers = new HttpHeaders()
    .set('Authorization', 'Bearer ' + accessToken);

    return this._http.post(environment.apiBaseUrl + 'user/get-filter-options', fd, { headers })
    .pipe(map(res => JSON.parse(JSON.stringify(res))));
  }

  getCareerSearchData(accessToken: string, jobSector: any, careerTitle: string, avgWage: any,  careerTrack: any, pageNo: number) {
    let fd = new FormData();
    
    fd.append("job_sector_id", jobSector);
    fd.append("career_title", careerTitle);
    fd.append("wages", avgWage);
    fd.append("career_track_id", careerTrack);
    //let page_no = pageNo;
    //console.log(pageNo);
     fd.append("page", ''+pageNo);

    let headers = new HttpHeaders()
    .set('Authorization', 'Bearer ' + accessToken);

    return this._http.post(environment.apiBaseUrl + 'user/explore-career-search', fd, { headers })
    .pipe(map(res => JSON.parse(JSON.stringify(res))));
  }
  allocateFavourite(apiKey: string, careeer_id: any, action_type: any,accessToken: string) {
    let fd = new FormData();
    //let fd = new FormData();
    fd.append("career_id", careeer_id);
    fd.append("action", action_type);

    // fd.append("password", formGroupData.password);

    let headers = new HttpHeaders()
    .set('Authorization', 'Bearer ' + accessToken);

    return this._http.post(environment.apiBaseUrl + 'user/user-career-tag', fd, { headers })
    .pipe(map(res => JSON.parse(JSON.stringify(res))));
  }
  getTrackbySectorData(accessToken: string, jobSector: any) {
    let fd = new FormData();
    fd.append("job_sector_id", jobSector);
    let headers = new HttpHeaders()
    .set('Authorization', 'Bearer ' + accessToken);
    return this._http.post(environment.apiBaseUrl + 'user/get-careertrack-sector', fd, { headers })
    .pipe(map(res => JSON.parse(JSON.stringify(res))));
  }
}
