import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExploreCareerComponent } from './explore-career.component';

describe('ExploreCareerComponent', () => {
  let component: ExploreCareerComponent;
  let fixture: ComponentFixture<ExploreCareerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExploreCareerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExploreCareerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
