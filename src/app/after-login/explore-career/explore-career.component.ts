import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AfterLoginService } from '../after-login.service';
import { ExploreCareerService } from './explore-career.service';
import { environment as env } from '../../../environments/environment';
declare var $:any;

@Component({
  selector: 'app-explore-career',
  templateUrl: './explore-career.component.html',
  styleUrls: ['./explore-career.component.css']
})
export class ExploreCareerComponent implements OnInit {
  public accessToken: string = '';
  public pageData: any = [];
  public filterData: any = [];
  public apiKey: string = env.apiKey;
  public currentPage: number = 1;
  public lastPage: number = 1;
  public paginationArr: any = [];
  public jobLevel: any = [];
  public jobZone: any = [];
  public careerTitle: string = '';
  public avgWage: any = [];
  public careerTrack: any = [];
  public jobSector: any = [];
  public paginContent: any = [];
  public paginationArrNew: any = [];
  public start: any = [];
  public end: any = [];
  public last: any = [];

  
  public careerResultForm: FormGroup= this.formBuilder.group({
  });  



  constructor(private _service:ExploreCareerService,private _router:Router,private _afterLoginService:AfterLoginService,private _title:Title,private formBuilder:FormBuilder,private _toastr:ToastrService) { }

  ngOnInit(): void {
    try {
      let accessToken = localStorage.getItem(btoa('access_token'));

      if(typeof accessToken !== 'undefined' && accessToken != null && accessToken != '') {
        this.accessToken = atob(accessToken);
        this.getFilterMasterData();
        this.getCareerData(this.currentPage);
      }
      else
        this._router.navigateByUrl('/login');
    } catch (error) {
      this._router.navigateByUrl('/login');
    }
    setTimeout(() =>{ $(".lazy").lazy(); }, 1000);
  }

  getFilterMasterData() {
    this._service.getFilterMaster(this.accessToken)
    .subscribe((resp) => {
      
      if(resp.status==200)
      {
        this.filterData = resp.data;
        
        
      
      }      
    }, err => {
      console.log(err);
    });
  }
  getCareerData(pageNo: number) {
    $('#filterCareer').attr('disabled','true');
    $('#filterCareer').html('<i class="fa fa-spinner icon-spin"></i> Filter');
    this._service.getCareerSearchData(this.accessToken, this.jobSector, this.careerTitle, this.avgWage, this.careerTrack, pageNo)
    .subscribe((resp) => {
      $('#filterCareer').removeAttr('disabled');
      $('#filterCareer').html('Filter');
      if(resp.status==200)
      {
        this.pageData = resp.data;
        this.currentPage = resp.data.current_page;
        this.lastPage = resp.data.last_page;
        let paginationLength = resp.data.last_page;

        this.paginationArr = [];

        //for(let i=1; i<=paginationLength; i++) {
          //this.paginationArr.push(i);
        //}
        
        //this.paginContent = this.paginate(this.pageData );

        this.paginationArrNew = [];
        this.start = ( this.pageData.current_page -2 )  > 0 ?  (this.pageData.current_page -2)  : 1 ;
        this.last = Math.ceil(this.pageData.total /this.pageData.per_page) ;
        this.end = ((this.pageData.current_page +2)  < this.last)  ?  (this.pageData.current_page +2 ) : this.last ;
        for(let i= this.start ; i <= this.end ; i++){
          this.paginationArrNew.push(i);
        }
     
      
      }      
    }, err => {
      console.log(err);
    });
  }

  paginationFirstClicked(): void {
    if(this.currentPage == 1) return;

    this.getCareerData(1);
  }
  paginationLastClicked(): void {
    if(this.currentPage ==this.lastPage) return;

    this.getCareerData(this.lastPage);
  }
  paginationClicked(pageno:any):void {
    this.getCareerData(pageno);
  }

  paginationPreviousClicked(): void {
    if(this.currentPage == 1) return;

    this.getCareerData(this.currentPage - 1);
  }
  paginationNextClicked(): void {
    if(this.currentPage == this.paginationArrNew.length) return;

    this.getCareerData(this.currentPage + 1);
  }


  fieldsChange(values:any):void {
   
    if(values.currentTarget.checked)
    {
      var career_id = values.target.defaultValue;
      var action_type = 'add';
      this._service.allocateFavourite(this.apiKey, career_id, action_type, this.accessToken)
      .subscribe(resp => {      
        
        if(resp.status == 200) {
         
          if(resp.response!='')
          {
            this._toastr.success(resp.response); 
            this.getCareerData(this.currentPage);
          }
          
          else{
            return;
          }
          
        }
      }, err => {     
        this._toastr.error(err.message);
      });
    }
    else{
     
      var career_id = values.target.defaultValue;
      var action_type = 'remove';
      this._service.allocateFavourite(this.apiKey, career_id, action_type, this.accessToken)
      .subscribe(resp => {      
        
        if(resp.status == 200) {
          
         
          if(resp.response!='')
          {
            this._toastr.success(resp.response); 
            this.getCareerData(this.currentPage);
            
          }
          
          else{
            return;
          }
          
        }
      }, err => {     
        this._toastr.error('Something went wrong.');
      });
    }
  }


  searchCareer(): void {
    
    this.getCareerData(1);
  }

  /* For pagination Building */
  paginate (data:any){
    let pagination = '';
    let cDisabled = data.prev_page_url == null ? 'disabled' : '';
    let lDisabled = data.next_page_url == null ? 'disabled' : '';

    let start = ( data.current_page -2 )  > 0 ?  (data.current_page -2)  : 1 ;
    let last = Math.ceil(data.total /data.per_page) ;
    let end = ((data.current_page +2)  < last)  ?  (data.current_page +2 ) : last ;
    
    pagination += `
    <li><a href="javascript:void(0);" (click)="paginationFirstClicked()"><img src="assets/images/lft-arrow.png"></a></li>
    <li><a href="javascript:void(0);" (click)="paginationFirstClicked()"><img src="assets/images/lft-arrow.png"></a></li>           
       `;
  
    if(   start > 1   ){
        pagination +=    `
        <li><a href="javascript:void(0);" (click)="paginationFirstClicked()">1</a></li>
        <li><a href="javascript:void(0);" >...</a></li>
            
            `;
        }  

        for(let i= start ; i <= end ; i++){
            var active = data.current_page == i ? 'active' : false;
            if(i==data.current_page)
            {
                pagination += ` 
                <li><a href="javascript:void(0);" class="active" title="Page ${i}" >${i}</a></li>
                  
                  `;
            }
            else
            {
              pagination += ` 
              <li><a href="javascript:void(0);" title="Page ${i}" (click)="paginationClicked(${i})">${i}</a></li>
                  
                  `;
            }
      
            
        }
     

    if(end < last){
        pagination +=    `
        <li><a href="javascript:void(0);" >...</a></li>
        <li><a href="javascript:void(0);" (click)="paginationLastClicked()"><img src="assets/images/rgt-arrow.png"></a></li>
            
            `;
        }     

    pagination  += `      
                    <li><a href="javascript:void(0);" (click)="paginationLastClicked()"><img src="assets/images/rgt-arrow.png"></a></li>
                    <li><a href="javascript:void(0);" (click)="paginationLastClicked()"><img src="assets/images/rgt-arrow.png"></a></li> 
                    `;                      
    //document.getElementById("pagination").innerHTML = pagination;   
    
    return pagination;
}
changeCountry(data:any){

  console.log(data.target.value);
  this._service.getTrackbySectorData(this.accessToken, data.target.value)
    .subscribe((resp) => {
     
      if(resp.status==200)
      {
        this.filterData.career_tracks = resp.response;
      }      
    }, err => {
      console.log(err);
    });
}

}
