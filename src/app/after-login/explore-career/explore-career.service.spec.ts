import { TestBed } from '@angular/core/testing';

import { ExploreCareerService } from './explore-career.service';

describe('ExploreCareerService', () => {
  let service: ExploreCareerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ExploreCareerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
