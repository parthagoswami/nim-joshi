import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AfterLoginService } from '../after-login.service';
import { ChangePasswordService } from './change-password.service';
import { environment as env } from '../../../environments/environment';
declare var $:any;

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  public accessToken: string = '';
  public pageData: any = [];
  public isPasswordSaveClicked: boolean = false;
  public apiKey: string = env.apiKey; 
  public matchPwdStat: number = 0;
  public matchnewPwdStat: number = 0;
  public matchconfirmPwdStat: number = 0;
  
  public changePwdForm: FormGroup = this.formBuilder.group({
    'password': ['', [Validators.required, Validators.pattern(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$/)]],
    'new_password': ['', [Validators.required, Validators.pattern(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$/)]],
    'confirm_password': ['', [Validators.required, Validators.pattern(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$/)]]
    
  })

  constructor(private _service:ChangePasswordService,private _router:Router,private _title:Title,private _afterLoginService: AfterLoginService,private formBuilder:FormBuilder,private _toastr:ToastrService) { }

  ngOnInit(): void {
    try {
      let accessToken = localStorage.getItem(btoa('access_token'));

      if(typeof accessToken !== 'undefined' && accessToken != null && accessToken != '') {
        this.accessToken = atob(accessToken);
      }
      else
        this._router.navigateByUrl('/login');
    } catch (error) {
      this._router.navigateByUrl('/login');
    }

    this._title.setTitle(`${env.changePwdTitle}`);
    $('#hideshowpwd').on('click', function(){
      let current_tp = $('#password').attr('type');
      if(current_tp=='text')
      {
        $('#password').attr('type','password');
        $('#hideshowpwd').html('<i class="fa fa-eye-slash" aria-hidden="true"></i>');
      }
      else
      {
        $('#password').attr('type','text');
        $('#hideshowpwd').html('<i class="fa fa-eye" aria-hidden="true"></i>');
      }
      
    });
    $('#hideshowpwdnew').on('click', function(){
      let current_tp = $('#new_password').attr('type');
      if(current_tp=='text')
      {
        $('#new_password').attr('type','password');
        $('#hideshowpwdnew').html('<i class="fa fa-eye-slash" aria-hidden="true"></i>');
      }
      else
      {
        $('#new_password').attr('type','text');
        $('#hideshowpwdnew').html('<i class="fa fa-eye" aria-hidden="true"></i>');
      }
      
    });
    $('#hideshowpwdconf').on('click', function(){
      let current_tp = $('#confirm_password').attr('type');
      if(current_tp=='text')
      {
        $('#confirm_password').attr('type','password');
        $('#hideshowpwdconf').html('<i class="fa fa-eye-slash" aria-hidden="true"></i>');
      }
      else
      {
        $('#confirm_password').attr('type','text');
        $('#hideshowpwdconf').html('<i class="fa fa-eye" aria-hidden="true"></i>');
      }
      
    });
  }
  public ChangePwdFormSubmitted(): void {
    this.isPasswordSaveClicked = true;
    if(this.changePwdForm.invalid) {
      return;
    }
    if(this.changePwdForm.value.password!='' && this.changePwdForm.value.new_password!='' && this.changePwdForm.value.password==this.changePwdForm.value.new_password)
    {
      this.matchnewPwdStat = 1;
      return;
    }
    else if(this.changePwdForm.value.password!='' && this.changePwdForm.value.confirm_password!='' && this.changePwdForm.value.password==this.changePwdForm.value.confirm_password)
    {
      this.matchnewPwdStat = 0;
      this.matchconfirmPwdStat = 1;
      return;
    }
    else if(this.changePwdForm.value.new_password!='' && this.changePwdForm.value.confirm_password!='' && this.changePwdForm.value.new_password!=this.changePwdForm.value.confirm_password)
    {
      this.matchnewPwdStat = 0;
      this.matchconfirmPwdStat = 0;
      this.matchPwdStat = 1;
      return;
    }
    this.matchnewPwdStat = 0;
    this.matchconfirmPwdStat = 0;
    this.matchPwdStat = 0;
    
    $('#changepwdbtn').attr('disabled','true');
    $('#changepwdbtn').html('<i class="fa fa-spinner icon-spin"></i> Submit');
    this._service.changePwdFormSubmit(this.apiKey, this.accessToken, this.changePwdForm.value)
    .subscribe(resp => {
      $('#changepwdbtn').removeAttr('disabled');
      $('#changepwdbtn').html('Submit');
      console.log(resp);
      if(resp.status == 200) {
        if(resp.errors!=undefined)
        {
          this._toastr.error(resp.errors);
        }
        else
        {
          this._toastr.success(resp.response);
          this.changePwdForm.reset();
          this.isPasswordSaveClicked = false;
        }
                 
      } else {       
        this._toastr.error(resp.errors); 
      }


    }, err => {
      console.log(err);
      //this.loginMsg = err.message;
      this._toastr.error(err.message);
    });
    console.log('Change Password Submit');

  }

}
