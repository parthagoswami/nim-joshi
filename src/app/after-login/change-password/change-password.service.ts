import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ChangePasswordService {

  constructor(private _http:HttpClient) { }
  changePwdFormSubmit(apiKey: string, accessToken: string, formGroupData: any) {
    let fd = new FormData();
    fd.append("password", formGroupData.password);    
    fd.append("new_password", formGroupData.new_password);
    fd.append("confirm_password", formGroupData.confirm_password);
    let headers = new HttpHeaders()
    .set('Authorization', 'Bearer ' + accessToken);  
    return this._http.post(environment.apiBaseUrl + 'user/change-password-submit', fd, { headers })
    .pipe(map(res => JSON.parse(JSON.stringify(res))));
  }
}
