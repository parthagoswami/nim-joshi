import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AfterLoginService } from './after-login.service';
import { environment } from '../../environments/environment';
declare var $:any;

@Component({
  selector: 'app-after-login',
  templateUrl: './after-login.component.html',
  styleUrls: ['./after-login.component.css']
})
export class AfterLoginComponent implements OnInit {
  public apiKey: string = environment.apiKey; 
  public accessToken: string = '';
  public userName: string = '';
  public userRole: any;
  public userId: any;
  public userPath: any = '';
  public loggedInData: any = [];
  public imagePath: any;
  public imgURL: any = 'assets/images/user.jpg';
  public loggedInName: string = '';

  constructor( private _afterLoginService:AfterLoginService, private _router:Router, private _toastr:ToastrService,  private _cdr:ChangeDetectorRef) {
    this._afterLoginService.getLoggedInNameSubject().subscribe(resp => {
      this.loggedInName = resp;
      this._cdr.detectChanges();
      
    }); 
   }

  ngOnInit(): void {
    try {
    let accessToken = localStorage.getItem(btoa('access_token'));

   
    let userId = localStorage.getItem('user_id');
    let userName = localStorage.getItem('user_name');
   
    let userRole = localStorage.getItem('user_role');
    let profileImage = localStorage.getItem('profile_image');
    this._afterLoginService.setLoggedInNameSubject(userName); 
    this._afterLoginService.setLoggedOutNameSubject('Log Out');

   
     
    if(userRole=='' || userRole =='undefined' || userRole==null)
    {
      this._afterLoginService.setLoggedInNameSubject('Register');
      this._afterLoginService.setLoggedOutNameSubject('Sign In'); 
      this.logoutClicked();
    }
    if(typeof accessToken !== 'undefined' && accessToken != null && accessToken != '') {
      this.accessToken = atob(accessToken); 
      if(typeof userName !== 'undefined' && userName != null && userName != '') {
        this.userId = userId;
        this.userName = userName;
        this.userRole = userRole; 

       

        if(typeof profileImage !== 'undefined' && profileImage != null && profileImage != 'null' && profileImage != '')          
        {
          
          this.imgURL = environment.apiFileUrl+profileImage;
        }
        
        if(this.userRole == 1)
        {
          this.userPath ="student";
          this._afterLoginService.setLoggedInUserRoleSubject('student');
        }       
        if(this.userRole == 2)
        {
          this.userPath ="professional";
          this._afterLoginService.setLoggedInUserRoleSubject('professional');
        }
       
      }             
     
    }
    else
      this._router.navigateByUrl('/login');
  } catch (error) {
    this._router.navigateByUrl('/login');
  }

  $('#menu-icon').on('click', function(){
    $('.mob').toggleClass('expand');
    return false;
  });
  $('.mobmenu').on('click', function(){
    $('.mob').toggleClass('expand');
  });

  }


  logoutClicked() {

    this._afterLoginService.logOut(this.apiKey, this.accessToken)
    .subscribe(res => {
      
      localStorage.removeItem(btoa('access_token'));
      localStorage.removeItem('userInfo');
      localStorage.clear();
      localStorage.removeItem('user_id');
      localStorage.removeItem('user_name');
      localStorage.removeItem('user_role');
      localStorage.removeItem('profile_image');        
      
      window.location.reload();
    },err => {
      console.log(err);
      localStorage.removeItem(btoa('access_token'));
      localStorage.removeItem('user_name');
      localStorage.removeItem('user_role'); 
      window.location.reload();
    });    
    
  }

  uploadProfileImage(e:any)
  {
    if (e.target.files[0].length === 0)
      return;
 
    var mimeType = e.target.files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this._toastr.error("Only images are supported.");
      return;
    }
 
    var reader = new FileReader();
    this.imagePath = e.target.files[0];  
    reader.readAsDataURL(e.target.files[0]); 
    reader.onload = (_event) => { 
    this.imgURL = reader.result; 
    }

    this.saveProfileImage();
  }

  saveProfileImage(): void{
    this._afterLoginService.proImgSubmitted(this.accessToken, this.imagePath)
    .subscribe(resp => {
      console.log(resp);
      if(resp.status == 200) {
        localStorage.setItem('profile_image', resp.data.profile_image);
        this._toastr.success(resp.response);
        
      } else {
                
        //this._toastr.error(resp.response);
        this._toastr.error('Some error occured.')
      }


    }, err => {
      console.log(err);
      // this.responseMsg = err.message;
      this._toastr.error("Something went wrong. Please try again later.");
    });
  }

}
