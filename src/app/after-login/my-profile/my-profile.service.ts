import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MyProfileService {

  constructor(private _http: HttpClient) { }

  getUserData(accessToken: string) {
    let fd = new FormData();

    // fd.append("password", formGroupData.password);

    let headers = new HttpHeaders()
    .set('Authorization', 'Bearer ' + accessToken);

    return this._http.post(environment.apiBaseUrl + 'auth/my-profile-data', fd, { headers })
    .pipe(map(res => JSON.parse(JSON.stringify(res))));
  }
  duplicateEmail(apiKey: string, formGroupData: any, accessToken: string) {
    let fd = new FormData();
    fd.append("email", formGroupData.email);
    
    let headers = new HttpHeaders()
    .set('X-Auth-Token', apiKey) 
    .set('Authorization', 'Bearer ' + accessToken);   

    return this._http.post(environment.apiBaseUrl + 'auth/duplicate-email-user', fd, { headers })
    .pipe(map(res => JSON.parse(JSON.stringify(res))));
  }
  profileSubmit(apiKey: string, formGroupDataProfile: any,accessToken: string) {
    
    let fd = new FormData();
    fd.append("first_name", formGroupDataProfile.first_name);
    fd.append("last_name", formGroupDataProfile.last_name);
    fd.append("email", formGroupDataProfile.email);
    fd.append("alternate_email", formGroupDataProfile.email_altername);
    fd.append("gender", formGroupDataProfile.gender);
    fd.append("contact_country_code", '');
    fd.append("contact_no", formGroupDataProfile.contact_no);
    fd.append("address", formGroupDataProfile.address);
    fd.append("user_bio", formGroupDataProfile.user_bio);
   
    
    let headers = new HttpHeaders()
    .set('Authorization', 'Bearer ' + accessToken);  

    return this._http.post(environment.apiBaseUrl + 'user/update_profile', fd, { headers })
    .pipe(map(res => JSON.parse(JSON.stringify(res))));
  }

  assessmentSubmit(apiKey: string, formGroupDataAssessment: any,accessToken: string) {
    
    let u_interests = '';
    for(var i=0;i<formGroupDataAssessment.user_work_interests.length;i++)
    {
      if(u_interests!='')
      {
        u_interests = u_interests+','+formGroupDataAssessment.user_work_interests[i].id;
      }
      else{
        u_interests =formGroupDataAssessment.user_work_interests[i].id;
      }
    }
    let u_values = '';
    for(var i=0;i<formGroupDataAssessment.user_work_values.length;i++)
    {
      if(u_values!='')
      {
        u_values = u_values+','+formGroupDataAssessment.user_work_values[i].id;
      }
      else{
        u_values =formGroupDataAssessment.user_work_values[i].id;
      }
    }
    
    let fd = new FormData();
    fd.append("sustainable_wage", formGroupDataAssessment.sustainable_wage);
    fd.append("user_work_interests", u_interests);
    fd.append("user_work_values", u_values);
    
   
    
    let headers = new HttpHeaders()
    .set('Authorization', 'Bearer ' + accessToken);  

    return this._http.post(environment.apiBaseUrl + 'user/update_self_assesment', fd, { headers })
    .pipe(map(res => JSON.parse(JSON.stringify(res))));
  }
  basicEducationSubmit(apiKey: string, formGroupDataVasicEdu: any,accessToken: string){
   
    let fd = new FormData();
    fd.append("hisghest_qualification", formGroupDataVasicEdu.highest_qualification);
    fd.append("hisghest_qualification_grade", formGroupDataVasicEdu.highest_qualification_grade);
    fd.append("hs_qualification", formGroupDataVasicEdu.hs_qualification_title);
    fd.append("hs_qualification_grade", formGroupDataVasicEdu.hs_qualification_grade);
    fd.append("school_qualification", formGroupDataVasicEdu.school_qualification_title);
    fd.append("school_qualification_grade", formGroupDataVasicEdu.school_qualification_grade);
    
   
    
    let headers = new HttpHeaders()
    .set('Authorization', 'Bearer ' + accessToken);  

    return this._http.post(environment.apiBaseUrl + 'user/update_basic_qualification', fd, { headers })
    .pipe(map(res => JSON.parse(JSON.stringify(res))));

  }
  certificateEducationSubmit(apiKey: string, formGroupDataCertificate: any,accessToken: string){
    
    
    let headers = new HttpHeaders()
    .set('Authorization', 'Bearer ' + accessToken);  

    return this._http.post(environment.apiBaseUrl + 'user/update-certificates', formGroupDataCertificate, { headers })
    .pipe(map(res => JSON.parse(JSON.stringify(res))));

  }
  diplomaEducationSubmit(apiKey: string, formGroupDataDiploma: any,accessToken: string){
    
    
    let headers = new HttpHeaders()
    .set('Authorization', 'Bearer ' + accessToken);  

    return this._http.post(environment.apiBaseUrl + 'user/update-diplomas', formGroupDataDiploma, { headers })
    .pipe(map(res => JSON.parse(JSON.stringify(res))));

  }

  experienceSubmit(apiKey: string, formGroupDataExperience: any,accessToken: string){
    
    
    let headers = new HttpHeaders()
    .set('Authorization', 'Bearer ' + accessToken);  

    return this._http.post(environment.apiBaseUrl + 'user/update_work_experiences', formGroupDataExperience, { headers })
    .pipe(map(res => JSON.parse(JSON.stringify(res))));

  }

  
}
