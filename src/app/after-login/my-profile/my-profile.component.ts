import { Component, NgZone, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { AfterLoginService } from '../after-login.service';
import { MyProfileService } from './my-profile.service';
import { environment as env } from '../../../environments/environment';
import { FormBuilder, FormArray, FormGroup, Validators, FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { IDropdownSettings, } from 'ng-multiselect-dropdown';
import { Location } from '@angular/common';
declare var $:any;


@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.component.html',
  styleUrls: ['./my-profile.component.css'],
  
})
export class MyProfileComponent implements OnInit {

  public accessToken: string = '';
  public pageData: any = [];
  public maleChecked: string = '';
  public femaleChecked: string = '';
  public nonBinaryChecked: string = '';
  public isProfileSaveClicked: boolean = false;
  public isProfileEducationSaveClicked: boolean = false;
  public isAssessmentSaveClicked: boolean = false;
  public isProfileCertificateSaveClicked: boolean = false;
  public isProfileDiplomaSaveClicked: boolean = false;
  //public userAddress: string = '';
  public duplicateEmailStat: any = [];
  public duplicateEmailMsg: string = '';
  public apiKey: string = env.apiKey; 
  
  public alternateEmailAdd: any = [];
  public alternateEmailStat: any = [];
  public alternateEmailMsg: string = '';
  public certificateValues: any = [];
  public certificatearr: any = [];
  public certificatearrVal: any = [];
  public requiredField: boolean = false;
  public requiredField2: boolean = false;
  public isExperienceSaveClicked: boolean = false;
  public totalExpYear: string = '0';
  public totalExpMonth: string = '0';
  public userName: string = '';
 

 
  

  public profilePersonalForm: FormGroup = this.formBuilder.group({
    'first_name': ['', [Validators.required, Validators.pattern(/^([a-zA-Z]+\s)*[a-zA-Z]+$/)]],
    'last_name': ['', [Validators.required, Validators.pattern(/^([a-zA-Z]+\s)*[a-zA-Z]+$/)]],
    'gender': ['', [Validators.required]],
    'email': ['', [Validators.required, Validators.pattern(/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i)]],
    'email_altername': ['', [Validators.pattern(/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i)]],
    'contact_no': ['', [Validators.required, Validators.pattern(/^((\\+91-?)|0)?[0-9]{10}$/)]],
    'address': ['', [Validators.required]],
    'user_bio': [''],
  })

  public profileEducationForm: FormGroup = this.formBuilder.group({
    'highest_qualification': ['', [Validators.required]],
    'highest_qualification_grade': ['', [Validators.required]],
    'hs_qualification_title': [''],
    'hs_qualification_grade': [''],
    'school_qualification_grade':[''],
    'school_qualification_title':['']
    
  })
 
  public assessmentForm: FormGroup = this.formBuilder.group({
    'sustainable_wage': ['', [Validators.required]],
    'user_work_interests': ['', [Validators.required]],
    'user_work_values': ['', [Validators.required]]
  
  })

  public certificateForm: FormGroup;  
  public diplomaForm: FormGroup;
  public experienceForm: FormGroup;
  

  

  constructor(private _service: MyProfileService,private _router: Router,private _afterLoginService: AfterLoginService,private _title: Title, private formBuilder:FormBuilder, private _toastr:ToastrService, private location:Location ) { 
    this.certificateForm = this.formBuilder.group({  
      certificates: this.formBuilder.array([]) 
    });
    this.diplomaForm = this.formBuilder.group({  
      diplomas: this.formBuilder.array([]) 
    });  
    this.experienceForm = this.formBuilder.group({  
      experiences: this.formBuilder.array([]) 
    });    
  }
  certificates() : FormArray {  
    return this.certificateForm.get("certificates") as FormArray  
  }  
  newCertificate(): FormGroup {  
    return this.formBuilder.group({  
      'course_type_id': '', 
      'cert_tid': '',  
      'cert_qualification_title': ['', [Validators.required]],  
      'cert_qualification_grade': ['', [Validators.required]],
    })  
  } 
  
  addCertificate() {  
    this.certificates().push(this.newCertificate());  
  }  
  removeCertificate(i:number) {  
    this.certificates().removeAt(i);  
  }  


  diplomas() : FormArray {  
    return this.diplomaForm.get("diplomas") as FormArray  
  }  
  newDiploma(): FormGroup {  
    return this.formBuilder.group({  
      'diploma_course_types_id': '', 
      'diploma_tid': '',  
      'diploma_qualification_title': ['', [Validators.required]],  
      'diploma_qualification_grade': ['', [Validators.required]],
    })  
  } 
  
  addDiploma() {  
    this.diplomas().push(this.newDiploma());  
  }  
  removeDipoma(j:number) {  
    this.diplomas().removeAt(j);  
  }  


  experiences() : FormArray {  
    return this.experienceForm.get("experiences") as FormArray  
  }  
  newExperience(): FormGroup {  
    return this.formBuilder.group({  
      'exp_id':'',
      'company_name': ['', [Validators.required]], 
      'designation': ['', [Validators.required]],  
      'job_role': ['', [Validators.required]],  
      'work_from': ['', [Validators.required]],
      'work_to': ['', [Validators.required]],
      'job_description': ['', [Validators.required]]
    })  
  } 
  
  addExperience() {  
    this.experiences().push(this.newExperience());  
  }  
  removeExperience(k:number) {  
    this.experiences().removeAt(k);  
  }  
  
  
  public dropdownList: any = [];
  public dropdownSettings:IDropdownSettings={};
  public dropdownListVal: any = [];
  public dropdownSettingsVal:IDropdownSettings={};
  public selectedItems1=[];
  public selectedItems2=[];
  public StatusInterestList: boolean = false;
  public StatusValueList: boolean = false;

  ngOnInit(): void {
    
    try {
      let accessToken = localStorage.getItem(btoa('access_token'));

      if(typeof accessToken !== 'undefined' && accessToken != null && accessToken != '') {
        this.accessToken = atob(accessToken);
        this.getProfileData();
      }
      else
        this._router.navigateByUrl('/login');
    } catch (error) {
      this._router.navigateByUrl('/login');
    }

    this._title.setTitle(`My Account - ${env.profileTitle}`);

    setTimeout(() =>{ $(".lazy").lazy(); }, 1000);
    
      
    
      

      
  }


  getProfileData() {
    this._service.getUserData(this.accessToken)
    .subscribe((resp) => {
      
      if(resp.status==200)
      {
        this.pageData = resp.data;
       
        this.profilePersonalForm.patchValue({
          first_name:this.pageData?.user_data?.get_user_profile?.first_name,
          last_name:this.pageData?.user_data?.get_user_profile?.last_name,
          email:this.pageData?.user_data?.email,
          contact_no:this.pageData?.user_data?.get_user_profile?.contact_no,
          email_altername:this.pageData?.user_data?.get_user_profile?.alternate_email,
          address:this.pageData?.user_data?.get_user_profile?.address,
          user_bio:this.pageData?.user_data?.get_user_profile?.user_bio,
          gender:this.pageData?.user_data?.get_user_profile?.gender
        })

        

        this.profileEducationForm.patchValue({
          highest_qualification:this.pageData?.education?.highest?.qualification_title,
          highest_qualification_grade:this.pageData?.education?.highest?.qualification_grade,
          hs_qualification_title:this.pageData?.education?.high_school?.qualification_title,
          hs_qualification_grade:this.pageData?.education?.high_school?.qualification_grade,
          school_qualification_title:this.pageData?.education?.tenth_school?.qualification_title,
          school_qualification_grade:this.pageData?.education?.tenth_school?.qualification_grade,
          
        })
        for( var i=0;i<this.pageData?.education?.certificates.length;i++)
        {
          let element:HTMLElement = document.getElementById('auto_trigger') as HTMLElement;
          if(element){
            element.click();
            }
        }
        for( var i=0;i<this.pageData?.education?.diplomas.length;i++)
        {
          let element_diploma:HTMLElement = document.getElementById('auto_trigger_diploma') as HTMLElement;
          if(element_diploma){
            element_diploma.click();
            }
        }

        for( var i=0;i<this.pageData?.user_experiences.length;i++)
        {
          let element:HTMLElement = document.getElementById('auto_trigger_experience') as HTMLElement;
          if(element){
            element.click();
            }
        }
        
        
        
        
        if(this.pageData?.education?.certificates.length>0)
        {
          this.certificates().setValue(this.pageData?.education?.certificates);
        }
        if(this.pageData?.education?.diplomas.length>0)
        {
          this.diplomas().setValue(this.pageData?.education?.diplomas);
        }
       
        if(this.pageData?.user_experiences.length>0)
        {
          this.experiences().setValue(this.pageData?.user_experiences);
        }
        if(this.pageData?.user_data?.total_exp_year!=null)
        {
          this.totalExpYear = this.pageData?.user_data?.total_exp_year;
        }
        if(this.pageData?.user_data?.total_exp_month!=null)
        {
          this.totalExpMonth = this.pageData?.user_data?.total_exp_month;
        }
        
        
        if(this.pageData?.work_interests)
        {
          this.StatusInterestList = true;
        }
        if(this.pageData?.work_values)
        {
          this.StatusValueList = true;
        }
        
     
        this.dropdownList = this.pageData?.work_interests;
        this.dropdownSettings = {
          idField: 'id',
          textField: 'title',
          enableCheckAll: false,
      };
      this.dropdownListVal = this.pageData?.work_values;
        this.dropdownSettingsVal = {
          idField: 'id',
          textField: 'title',
          enableCheckAll: false,
      };
      this.selectedItems1 = this.pageData?.user_work_interests;
      this.selectedItems2 = this.pageData?.user_work_values;
      this.assessmentForm = this.formBuilder.group({
        user_work_interests: [this.selectedItems1],
        user_work_values: [this.selectedItems2],
        sustainable_wage: this.pageData?.user_data.sustainable_wage
      });
      this.setStatus1();
      this.setStatus2();
      
      

      
      }      
    }, err => {
      console.log(err);
    });
  }

  setStatus1() {
    (this.selectedItems1.length > 0) ? this.requiredField = true : this.requiredField = false;
  }
  setStatus2() {
    (this.selectedItems2.length > 0) ? this.requiredField2 = true : this.requiredField2 = false;
  }
  setClass1() {
    this.setStatus1();
    if (this.selectedItems1.length > 0) { return 'validField' }
    else {  return 'invalidField' }
  }
  setClass2() {
    this.setStatus2();
    if (this.selectedItems2.length > 0) {   return 'validField' }
    else { return 'invalidField' }
  }
  onItemSelect1(item: any) {
    //Do something if required

    this.setClass1();
  }
  onItemSelect2(item: any) {
    //Do something if required
    this.setClass2();
  }
  

  public profilePersonalFormSubmitted(): void {
    
    this.isProfileSaveClicked = true;
    
  if(this.profilePersonalForm.invalid) {
      return;
   }
   this._service.duplicateEmail(this.apiKey, this.profilePersonalForm.value, this.accessToken)
    .subscribe(resp => {      
      
      if(resp.status == 200) {
        
        this.duplicateEmailMsg = resp.data.message;
        this.duplicateEmailStat = resp.data.status;
        if(resp.data.message!='')
        {
          this._toastr.error(resp.data.message,'Duplicate Email'); 
        }
        
        
        else{
          return;
        }
        
      }
    }, err => {     
      this._toastr.error(err.message);
    });
    if(this.profilePersonalForm.value.email==this.profilePersonalForm.value.email_altername)
    {
      this.alternateEmailStat=1;
      this.alternateEmailMsg='Altername Email can not be same as primary email.';
      this._toastr.error(this.alternateEmailMsg);
      return;
    }
    $('#personal_update').attr('disabled','true');
    $('#personal_update').html('<i class="fa fa-spinner icon-spin"></i> Update');
    this.alternateEmailStat=0;
    this.alternateEmailMsg='';

    this._service.profileSubmit(this.apiKey, this.profilePersonalForm.value, this.accessToken)
    .subscribe(resp => {      
      $('#personal_update').removeAttr('disabled');
      $('#personal_update').html('Update');
    if(resp.status == 200) { 
      if(resp.errors!=undefined)
      {
        let errMsg = '';
        $.each(resp.errors, function(name:any, value:any){   
          if(errMsg=='')
          {
            errMsg = value; 
          }
          else
          {
            errMsg = ', '+value; 
          }
                
          
        })
        this._toastr.error(errMsg); 
      } 
      else
      {
               
        this._toastr.success('Profile Updated Successfully.');
      
        this.userName = this.profilePersonalForm.value.first_name+' '+this.profilePersonalForm.value.last_name
        localStorage.setItem('user_name', this.userName);
        this._afterLoginService.setLoggedInNameSubject(this.userName); 
      }   
      
     
    }
  }, err => {   
    $("#expSubmit").removeAttr('disabled');        
    this._toastr.error(err.message);
  });

    
    
  }


  
  public profileEducationFormSubmitted(): void {
    
    this.isProfileEducationSaveClicked = true;
    
  if(this.profileEducationForm.invalid) {
      return;
   }
   $('#basicSubmit').attr('disabled','true');
    $('#basicSubmit').html('<i class="fa fa-spinner icon-spin"></i> Update');
   this._service.basicEducationSubmit(this.apiKey, this.profileEducationForm.value, this.accessToken)
    .subscribe(resp => {      
      $('#basicSubmit').removeAttr('disabled');
      $('#basicSubmit').html('Update');
    if(resp.status == 200) { 
      if(resp.errors!=undefined)
      {
        let errMsg = '';
        $.each(resp.errors, function(name:any, value:any){   
          if(errMsg=='')
          {
            errMsg = value; 
          }
          else
          {
            errMsg = ', '+value; 
          }
                
          
        })
        this._toastr.error(errMsg); 
      } 
      else
      {
        
        this._toastr.success('Basic Education Updated Successfully.');
        
      }   
      
     
    }
  }, err => {   
    $("#expSubmit").removeAttr('disabled');        
    this._toastr.error(err.message);
  });
   
   
    
    
  }

  public certificateSubmit(): void {
    
    this.isProfileCertificateSaveClicked = true;
    
  if(this.certificateForm.invalid) {
      return;
   }
    $('#certsubmitbtn').attr('disabled','true');
    $('#certsubmitbtn').html('<i class="fa fa-spinner icon-spin"></i> Update');
   this._service.certificateEducationSubmit(this.apiKey, this.certificateForm.value, this.accessToken)
    .subscribe(resp => {      
      $('#certsubmitbtn').removeAttr('disabled');
      $('#certsubmitbtn').html('Update');
    
    if(resp.status == 200) { 
      if(resp.errors!=undefined)
      {
        let errMsg = '';
        $.each(resp.errors, function(name:any, value:any){   
          if(errMsg=='')
          {
            errMsg = value; 
          }
          else
          {
            errMsg = ', '+value; 
          }
                
          
        })
        this._toastr.error(errMsg); 
      } 
      else
      {
        
        this._toastr.success('Certificates Updated Successfully.');
        this.certificates().setValue(resp.certificates);
        
      }   
      
     
    }
  }, err => {   
    $("#expSubmit").removeAttr('disabled');        
    this._toastr.error(err.message);
  });
  }

  public diplomaSubmit(): void {
    
    this.isProfileDiplomaSaveClicked = true;
    
    if(this.diplomaForm.invalid) {
        return;
    }
    $('#dipsubmitbtn').attr('disabled','true');
    $('#dipsubmitbtn').html('<i class="fa fa-spinner icon-spin"></i> Update');
    this._service.diplomaEducationSubmit(this.apiKey, this.diplomaForm.value, this.accessToken)
    .subscribe(resp => {      
      $('#dipsubmitbtn').removeAttr('disabled');
      $('#dipsubmitbtn').html('Update');
    
    if(resp.status == 200) { 
      if(resp.errors!=undefined)
      {
        let errMsg = '';
        $.each(resp.errors, function(name:any, value:any){   
          if(errMsg=='')
          {
            errMsg = value; 
          }
          else
          {
            errMsg = ', '+value; 
          }
                
          
        })
        this._toastr.error(errMsg); 
      } 
      else
      {
        
        this._toastr.success('Diplomas Updated Successfully.');
        this.diplomas().setValue(resp.diplomas);
        
      }   
      
     
    }
  }, err => {   
    //$("#expSubmit").removeAttr('disabled');        
    this._toastr.error(err.message);
    });
  }

  public assessmentFormSubmitted(): void {
    
    this.isAssessmentSaveClicked = true;
    
  if(this.assessmentForm.invalid) {
      return;
   }
   
   if(!this.assessmentForm.value.user_work_interests || this.assessmentForm.value.user_work_interests.length==0)
   {
    return;
   }
   if(!this.assessmentForm.value.user_work_values || this.assessmentForm.value.user_work_values.length==0)
   {
    return;
   }
   /*if (this.requiredField == false) {
    return;
   }
   if (this.requiredField2 == false) {
    return;
   }*/
   $('#asssubmitbtn').attr('disabled','true');
    $('#asssubmitbtn').html('<i class="fa fa-spinner icon-spin"></i> Update');
   this._service.assessmentSubmit(this.apiKey, this.assessmentForm.value, this.accessToken)
    .subscribe(resp => {      
      $('#asssubmitbtn').removeAttr('disabled');
      $('#asssubmitbtn').html('Update');
    if(resp.status == 200) { 
      if(resp.errors!=undefined)
      {
        let errMsg = '';
        $.each(resp.errors, function(name:any, value:any){   
          if(errMsg=='')
          {
            errMsg = value; 
          }
          else
          {
            errMsg = ', '+value; 
          }
                
          
        })
        this._toastr.error(errMsg); 
      } 
      else
      {
               
        this._toastr.success('Self Assessment Updated Successfully.');
       
      }   
      
     
    }
  }, err => {   
    $("#expSubmit").removeAttr('disabled');        
    this._toastr.error(err.message);
  });
  }

  public experienceFormSubmitted(): void {
    
    this.isExperienceSaveClicked = true;
      
    if(this.experienceForm.invalid) {
        return;
    }
    $('#expsubmitbtn').attr('disabled','true');
    $('#expsubmitbtn').html('<i class="fa fa-spinner icon-spin"></i> Update');
    this._service.experienceSubmit(this.apiKey, this.experienceForm.value, this.accessToken)
    .subscribe(resp => {   
      $('#expsubmitbtn').removeAttr('disabled');
      $('#expsubmitbtn').html('Update');   
    
    if(resp.status == 200) { 
      if(resp.errors!=undefined)
      {
        let errMsg = '';
        $.each(resp.errors, function(name:any, value:any){   
          if(errMsg=='')
          {
            errMsg = value; 
          }
          else
          {
            errMsg = ', '+value; 
          }
                
          
        })
        this._toastr.error(errMsg); 
      } 
      else
      {
        if(resp.experiences.length>0)
        {
          this.experiences().setValue(resp.experiences);
        }
        if(resp?.user_data?.total_exp_year!=null)
        {
          this.totalExpYear = resp?.user_data?.total_exp_year;
        }
        if(resp?.user_data?.total_exp_month!=null)
        {
          this.totalExpMonth = resp?.user_data?.total_exp_month;
        }
               
        this._toastr.success('Work Experience Updated Successfully.');
        
       
      }   
      
     
    }
  }, err => {   
    $("#expSubmit").removeAttr('disabled');        
    this._toastr.error(err.message);
  });
    
  }

  back(): void {
    this.location.back();
  }


}
