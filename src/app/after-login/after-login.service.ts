
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AfterLoginService {
  private _loggedInNameSubject = new Subject<any>();
  private _loggedInUserRoleSubject = new Subject<any>();
  private _loggedOutNameSubject = new Subject<any>();


  constructor(private _http:HttpClient) { }
  getLoggedInNameSubject() {
    return this._loggedInNameSubject.asObservable();
  }
  setLoggedInNameSubject(value: any) { 
    if (value) { 
      this._loggedInNameSubject.next(value); 
    } else { 
      this._loggedInNameSubject.next('Register'); 
    } 
  } 

  getLoggedInUserRoleSubject() {
    return this._loggedInUserRoleSubject.asObservable();
  }
  setLoggedInUserRoleSubject(value: any) { 
    if (value) { 
      this._loggedInUserRoleSubject.next(value); 
    } else { 
      this._loggedInUserRoleSubject.next(''); 
    } 
  } 

  getLoggedOutNameSubject() {
    return this._loggedOutNameSubject.asObservable();
  }
  setLoggedOutNameSubject(value: any) { 
    if (value) { 
      this._loggedOutNameSubject.next(value); 
    } else { 
      this._loggedOutNameSubject.next('Sign In'); 
    } 
  } 
 
  logOut(apiKey:string, accessToken: any) {
    let fd = new FormData();
    let headers = new HttpHeaders()
    .set('Authorization', 'Bearer ' + accessToken);
    return this._http.post(environment.apiBaseUrl + 'auth/logout',fd, { headers: headers })
    .pipe(map(res => JSON.parse(JSON.stringify(res))));
  }

  proImgSubmitted(accessToken: string, imageFile: any) {
    //console.log(imageFile);
    let fd = new FormData();
    fd.append("profile_image", imageFile, imageFile.name);    

    let headers = new HttpHeaders()
    .set('Authorization', 'Bearer ' + accessToken);

    return this._http.post(environment.apiBaseUrl + 'user/update-profile-image', fd, { headers })
    .pipe(map(res => JSON.parse(JSON.stringify(res))));
  }

}
