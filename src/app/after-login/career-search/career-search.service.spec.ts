import { TestBed } from '@angular/core/testing';

import { CareerSearchService } from './career-search.service';

describe('CareerSearchService', () => {
  let service: CareerSearchService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CareerSearchService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
