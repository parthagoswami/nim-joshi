import { Component, NgZone, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CareerSearchService } from './career-search.service';
import { environment as env } from '../../../environments/environment';
import { Title, Meta } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AfterLoginService } from '../after-login.service';
declare var $:any;


@Component({
  selector: 'app-career-search',
  templateUrl: './career-search.component.html',
  styleUrls: ['./career-search.component.css']
})
export class CareerSearchComponent implements OnInit {
  public accessToken: string = '';
  public pageData: any = [];
  public filterData: any = [];
  public apiKey: string = env.apiKey;
  public currentPage: number = 1;
  public lastPage: number = 1;
  public paginationArr: any = [];
  public jobSector: any = [];
  public avgWage: any = [];
  public minWageSlider:  any = '';
  public maxWageSlider:  any = '';

  public fliterWageMin:  number = 0;
  public filterWageMax:  number = 0;

  public start: any = [];
  public end: any = [];
  public last: any = [];

  public careerSearchResultForm: FormGroup= this.formBuilder.group({
  });  
  
  constructor(private _service:CareerSearchService,private _router:Router,private _afterLoginService:AfterLoginService,private _title:Title,private formBuilder:FormBuilder,private _toastr:ToastrService, private _ngZone: NgZone,private meta: Meta) { }

  ngOnInit(): void {
    try {
      let accessToken = localStorage.getItem(btoa('access_token'));

      if(typeof accessToken !== 'undefined' && accessToken != null && accessToken != '') {
        this.accessToken = atob(accessToken);
        this.getFilterCareerSearch();
        
      }
      else
        this._router.navigateByUrl('/login');
    } catch (error) {
      this._router.navigateByUrl('/login');
    }

    

  }
  initJquery(min_wage:any,max_wage:any)
  {
    
      let start_wage = 0;
      let end_wage = parseFloat(max_wage)+10;
        $( ()=> {
            $( "#slider-range" ).slider({
                range: true,
                min: min_wage,
                max: max_wage,
                values: [ min_wage, max_wage ],
                slide: ( event:any, ui:any )=> {
                    $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
                  
                      let minwage = ui.values[ 0 ];
                      let maxwage = ui.values[ 1 ];
                      this.minWageSlider = minwage;
                      this.maxWageSlider = maxwage;
                      
                  
                }
            });
          $( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
            " - $" + $( "#slider-range" ).slider( "values", 1 ) );
             
      } );
    setTimeout(() =>{ $(".lazy").lazy(); }, 1000);
  }

  getFilterCareerSearch() {
    this._service.getFilterCareerSearchOptions(this.accessToken)
    .subscribe((resp) => {
      
      if(resp.status==200)
      {
        this.filterData = resp.data;
        
        this.fliterWageMin = this.filterData.min_wage;
        this.filterWageMax = this.filterData.max_wage;
        this.minWageSlider = this.filterData.min_wage;;
        this.maxWageSlider = this.filterData.max_wage;
        this.initJquery(this.fliterWageMin, this.filterWageMax);
        this.getCareerData(this.currentPage);
        
      }      
    }, err => {
      //console.log(err);
    });
  }

  getCareerData(pageNo: number) {
    
    $('#filterCareerSearch').attr('disabled','true');
    $('#filterCareerSearch').html('<i class="fa fa-spinner icon-spin"></i> Filter');
    this._service.getCareerSearchData(this.accessToken, this.jobSector, this.minWageSlider,this.maxWageSlider, pageNo)
    .subscribe((resp) => {
      $('#filterCareerSearch').removeAttr('disabled');
      $('#filterCareerSearch').html('Filter');
      if(resp.status==200)
      {
        this.pageData = resp.data;
        this.currentPage = resp.data.current_page;
        this.lastPage = resp.data.last_page;
        let paginationLength = resp.data.last_page;

       
        this.paginationArr = [];
        this.start = ( this.pageData.current_page -2 )  > 0 ?  (this.pageData.current_page -2)  : 1 ;
        this.last = Math.ceil(this.pageData.total /this.pageData.per_page) ;
        this.end = ((this.pageData.current_page +2)  < this.last)  ?  (this.pageData.current_page +2 ) : this.last ;
        for(let i= this.start ; i <= this.end ; i++){
          this.paginationArr.push(i);
        }
       
        
     
      
      }      
    }, err => {
      //console.log(err);
    });
  }

  paginationFirstClicked(): void {
    if(this.currentPage == 1) return;

    this.getCareerData(1);
  }
  paginationLastClicked(): void {
    if(this.currentPage ==this.lastPage) return;

    this.getCareerData(this.lastPage);
  }
  paginationClicked(pageno:any):void {
    this.getCareerData(pageno);
  }
  paginationPreviousClicked(): void {
    if(this.currentPage == 1) return;

    this.getCareerData(this.currentPage - 1);
  }
  paginationNextClicked(): void {
    if(this.currentPage == this.paginationArr.length) return;

    this.getCareerData(this.currentPage + 1);
  }
  fieldsChange(values:any):void {
   
    if(values.currentTarget.checked)
    {
      var career_id = values.target.defaultValue;
      var action_type = 'add';
      this._service.allocateFavourite(this.apiKey, career_id, action_type, this.accessToken)
      .subscribe(resp => {      
        
        if(resp.status == 200) {
         
          if(resp.response!='')
          {
            this._toastr.success(resp.response); 
            this.getCareerData(this.currentPage);
          }
          
          
          else{
            return;
          }
          
        }
      }, err => {     
        this._toastr.error(err.message);
      });
    }
    else{
      
      var career_id = values.target.defaultValue;
      var action_type = 'remove';
      this._service.allocateFavourite(this.apiKey, career_id, action_type, this.accessToken)
      .subscribe(resp => {      
        
        if(resp.status == 200) {
         
          if(resp.response!='')
          {
            this._toastr.success(resp.response); 
            this.getCareerData(this.currentPage);
          }
          
          
          else{
            return;
          }
          
        }
      }, err => {     
        this._toastr.error('Something went wrong.');
      });
    }
  }

  searchCareer(): void {
    
    this.getCareerData(1);
  }


}
