import {  HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CareerSearchService {

  constructor(private _http:HttpClient) { }
  getFilterCareerSearchOptions(accessToken: string) {
    let fd = new FormData();
    let headers = new HttpHeaders()
    .set('Authorization', 'Bearer ' + accessToken);

    return this._http.post(environment.apiBaseUrl + 'user/get-career-search-filter-options', fd, { headers })
    .pipe(map(res => JSON.parse(JSON.stringify(res))));
  }
  getCareerSearchData(accessToken: string, jobSector: any, minWage: any, maxWage:any, pageNo: number)
  {
    //console.log(minWage);
    //console.log('test');
    let fd = new FormData();
    fd.append("sector_id", jobSector);
    fd.append("min_wage", minWage);
    fd.append("max_wage", maxWage);
  
    //let page_no = pageNo;
    //console.log(pageNo);
     fd.append("page", ''+pageNo);

    let headers = new HttpHeaders()
    .set('Authorization', 'Bearer ' + accessToken);

    return this._http.post(environment.apiBaseUrl + 'user/career-search', fd, { headers })
    .pipe(map(res => JSON.parse(JSON.stringify(res))));

  }
  allocateFavourite(apiKey: string, careeer_id: any, action_type: any,accessToken: string) {
    let fd = new FormData();
    //let fd = new FormData();
    fd.append("career_id", careeer_id);
    fd.append("action", action_type);

    // fd.append("password", formGroupData.password);

    let headers = new HttpHeaders()
    .set('Authorization', 'Bearer ' + accessToken);

    return this._http.post(environment.apiBaseUrl + 'user/user-career-tag', fd, { headers })
    .pipe(map(res => JSON.parse(JSON.stringify(res))));
  }
}
