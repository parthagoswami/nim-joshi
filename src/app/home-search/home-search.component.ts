import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AfterLoginService } from '../after-login/after-login.service';
import { HomeSearchService } from './home-search.service';
import { environment } from '../../environments/environment';
import { Title, Meta } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';
declare var $:any;


@Component({
  selector: 'app-home-search',
  templateUrl: './home-search.component.html',
  styleUrls: ['./home-search.component.css']
})
export class HomeSearchComponent implements OnInit {
  public accessToken: string = '';
  //public settingData: any = [];
  public apiFileUrl: string = environment.apiFileUrl;
  public pageData: any = [];
  public bannerUrl: any = [];
  public bannerCaption: any = [];
  public bannerAlt: string = '';
  public jobLevel: any = [];
  public jobSector: any = [];
  public minWageSlider:  any = '';
  public maxWageSlider:  any = '';
  public searchKeyword: any = '';
  public currentPage: number = 1;
  public lastPage: number = 1;
  public paginationArr: any = [];
  public fliterWageMin:  number = 0;
  public filterWageMax:  number = 0;
  public filterData: any = [];
  public careerData: any = [];
  public isLoggedIn : boolean=false;
  public apiKey: string = environment.apiKey;
  public userIdNo : any = [];
  public selectedJobLevel: string='';
  public selectedJobSector: any = [];
  public selectedWage: string='';
  public start: any = [];
  public end: any = [];
  public last: any = [];
  public userRole: any;
  public isLoggedInchk : boolean = false;
  
  constructor(private _searchService:HomeSearchService,private _afterLoginService:AfterLoginService, private _router:Router,private _title:Title, private _toastr:ToastrService,private meta: Meta) {
    let accessToken = localStorage.getItem(btoa('access_token'));
    if(typeof accessToken !== 'undefined' && accessToken != null && accessToken != '') {
      this.accessToken = atob(accessToken);
    }
    this.checkLogeedIn();
   
   }

  ngOnInit(): void {
    try{
      let accessToken = localStorage.getItem(btoa('access_token'));
     
      if(typeof accessToken !== 'undefined' && accessToken != null && accessToken != '') {
        this.accessToken = atob(accessToken); 
        this.isLoggedIn = true;
      }
      else
      {
        localStorage.removeItem('user_id');
      }
      let userId = localStorage.getItem('user_id');
     
      if(typeof userId !== 'undefined' && userId != null && userId != '')
      {
        this.userIdNo = btoa(userId);
      }
      
      this.searchKeyword = localStorage.getItem('searchKey');
      let jsect = localStorage.getItem('jbSector');
      if(typeof jsect !== 'undefined' && jsect != null && jsect != '')
      {
        this.jobSector = localStorage.getItem('jbSector');
        this.selectedJobSector = localStorage.getItem('jbSectorName');
        
      }
      
      
      this.getSettingData();
      this.getFilterCareerSearch();
      this.generateSearch();
    }catch(error){
      console.log(error);
    }
    setTimeout(() =>{ $(".lazy").lazy(); }, 1000);
  }

  getSettingData(){    
    this._searchService.getSearchSettingData()
    .subscribe((resp)=> {      
      if(resp.status == 200) {
        this.pageData = resp.data;
        this.bannerUrl =  resp.data.get_banner[0].banner_image;
        this.bannerCaption =  resp.data.get_banner[0].banner_image_caption;
        this.bannerAlt = resp.data.get_banner[0].title;
        this._title.setTitle(resp.data.page_meta_tag_title);
        this.meta.addTags([
          { name: 'keywords', content: resp.data.page_meta_tag_keywords },
          { name: 'description', content: resp.data.page_meta_tag_description },
          // ...
        ]);              
        
      }
    },err => {
      console.log(err);
    });
  }

  getFilterCareerSearch() {
    this._searchService.getFilterCareerSearchOptions(this.accessToken)
    .subscribe((resp) => {
      
      if(resp.status==200)
      {
        this.filterData = resp.data;
        this.fliterWageMin = this.filterData.min_wage;
        this.filterWageMax = this.filterData.max_wage;
        this.minWageSlider = this.filterData.min_wage;;
        this.maxWageSlider = this.filterData.max_wage;
        this.initJquery(this.fliterWageMin, this.filterWageMax);
        this.getCareerData(this.currentPage);
       
        
      }      
    }, err => {
      console.log(err);
    });
  }
  initJquery(min_wage:any,max_wage:any)
  {
    
      let start_wage = 0;
      let end_wage = parseFloat(max_wage)+10;
        $( ()=> {
            $( "#slider-range" ).slider({
                range: true,
                min: min_wage,
                max: max_wage,
                values: [ min_wage, max_wage ],
                slide: ( event:any, ui:any )=> {
                    $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
                  
                      let minwage = ui.values[ 0 ];
                      let maxwage = ui.values[ 1 ];
                      this.minWageSlider = minwage;
                      this.maxWageSlider = maxwage;
                      this.currentPage = 1;
                      this.getCareerData(this.currentPage);
                      this.selectedWage = "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ];
                  
                }
            });
          $( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
            " - $" + $( "#slider-range" ).slider( "values", 1 ) );
             
      } );
    setTimeout(() =>{ $(".lazy").lazy(); }, 1000);
  }
  progressbar(careers:any)
  {
    for(let j=0;j<=careers.length-1;j++)
    {
      var wage_match = parseFloat(careers[j].wage_match)/100;
      var int_match = parseFloat(careers[j].interested_match)/100;
      var value_match = parseFloat(careers[j].value_match)/100;
      

      let options = {
        startAngle: -1.55,
        size: 150,
        value: wage_match,
        fill: {gradient: ['#f8b914']}
      }
      $(".circle #bar0"+j).circleProgress(options).on('circle-animation-progress',
      (event:any, progress:any, stepValue:any) =>{
       /* if(parseFloat(careers[j].wage_match)>0)
        {
          $(event.currentTarget).parent().find("span").text(String(stepValue.toFixed(2).substr(2)) + "%");
        }
        else
        {
          $(event.currentTarget).parent().find("span").text(String(stepValue.toFixed(1).substr(2)) + "%");
        }*/
        
      });
      let options1 = {
        startAngle: -1.55,
        size: 150,
        value: int_match,
        fill: {gradient: ['#f8b914']}
      }
      $(".circle #bar1"+j).circleProgress(options1).on('circle-animation-progress',
      (event:any, progress:any, stepValue:any) =>{
      /*  if(parseFloat(careers[j].interested_match)>0)
        {
          $(event.currentTarget).parent().find("span").text(String(stepValue.toFixed(2).substr(2)) + "%");
        }
        else
        {
          $(event.currentTarget).parent().find("span").text(String(stepValue.toFixed(1).substr(2)) + "%");
        }*/
    
        
      });

      let options2 = {
        startAngle: -1.55,
        size: 150,
        value: value_match,
        fill: {gradient: ['#f8b914']}
      }
      $(".circle #bar2"+j).circleProgress(options2).on('circle-animation-progress',
      (event:any, progress:any, stepValue:any) =>{
        /*if(parseFloat(careers[j].value_match)>0)
        {
          $(event.currentTarget).parent().find("span").text(String(stepValue.toFixed(2).substr(2)) + "%");
        }
        else
        {
          $(event.currentTarget).parent().find("span").text(String(stepValue.toFixed(1).substr(2)) + "%");
        }*/
        
      });
      
    }
    
    
  }

  getCareerData(pageNo: number) {
    
     this._searchService.getCareerSearchData(this.accessToken, this.searchKeyword, this.jobLevel, this.jobSector, this.minWageSlider, this.maxWageSlider, this.userIdNo, pageNo)
     .subscribe((resp) => {
     
       if(resp.status==200)
       {
         this.careerData = resp.data;
         this.currentPage = resp.data.current_page;
         this.lastPage = resp.data.last_page;
         let paginationLength = resp.data.last_page;
 
        
        this.paginationArr = [];
        this.start = ( this.careerData.current_page -2 )  > 0 ?  (this.careerData.current_page -2)  : 1 ;
        this.last = Math.ceil(this.careerData.total /this.careerData.per_page) ;
        this.end = ((this.careerData.current_page +2)  < this.last)  ?  (this.careerData.current_page +2 ) : this.last ;
        for(let i= this.start ; i <= this.end ; i++){
          this.paginationArr.push(i);
        }
        
         setTimeout(() =>{ this.progressbar(this.careerData.careers); }, 1000);
         
         
      
       
       }      
     }, err => {
       console.log(err);
     });
   }

   paginationFirstClicked(): void {
    if(this.currentPage == 1) return;

    this.getCareerData(1);
  }
  paginationLastClicked(): void {
    if(this.currentPage ==this.lastPage) return;

    this.getCareerData(this.lastPage);
  }
  paginationClicked(pageno:any):void {
    this.getCareerData(pageno);
  }
  paginationPreviousClicked(): void {
    if(this.currentPage == 1) return;

    this.getCareerData(this.currentPage - 1);
  }
  paginationNextClicked(): void {
    if(this.currentPage == this.paginationArr.length) return;

    this.getCareerData(this.currentPage + 1);
  }
  onJobsectorSelected(value:any){
    
    let opt_text = $('#job_sector :selected').data('id');
    if(opt_text!=undefined)
    {
      this.selectedJobSector = opt_text;
    }
    else
    {
      this.selectedJobSector = '';
    }
    this.jobSector = value;
    this.currentPage = 1;
    this.getCareerData(this.currentPage);
  }
  onJoblevelSelected(value:any){
    let opt_text = $('#job_level :selected').data('id');
    if(opt_text!=undefined)
    {
      this.selectedJobLevel = opt_text;
    }
    else
    {
      this.selectedJobLevel = '';
    }
    
    this.jobLevel = value;
    this.currentPage = 1;
    this.getCareerData(this.currentPage);
  }

  
  AddFavorite(values:any):void {
    var career_id = values;
    var action_type = 'add';
    this._searchService.allocateFavourite(this.apiKey, career_id, action_type, this.accessToken)
    .subscribe(resp => {      
      
      if(resp.status == 200) {
       
        if(resp.response!='')
        {
          this._toastr.success(resp.response); 
          this.getCareerData(this.currentPage);
        }
        
        
        else{
          return;
        }
        
      }
    }, err => {     
      //this._toastr.error(err.message);
    });
  }
  removeFavorite(values:any):void {
    var career_id = values;
    var action_type = 'remove';
    this._searchService.allocateFavourite(this.apiKey, career_id, action_type, this.accessToken)
    .subscribe(resp => {      
      
      if(resp.status == 200) {
       
        if(resp.response!='')
        {
          this._toastr.success(resp.response); 
          this.getCareerData(this.currentPage);
        }
        
        
        else{
          return;
        }
        
      }
    }, err => {     
      //this._toastr.error(err.message);
    });
  }


  fieldsChange(values:any):void {
   
   
      var career_id = values;
      var action_type = 'add';
      this._searchService.allocateFavourite(this.apiKey, career_id, action_type, this.accessToken)
      .subscribe(resp => {      
        
        if(resp.status == 200) {
         
          if(resp.response!='')
          {
            this._toastr.success(resp.response); 
          }
          
          
          else{
            return;
          }
          
        }
      }, err => {     
        this._toastr.error(err.message);
      });
    
    
  }
  generateSearch(){
    $('#home_search').click(()=>{
      this.searchKeyword = $('#search_key').val();
      if(this.searchKeyword!='')
      {
        this.getCareerData(this.currentPage);
      }
      else
      {
        this.searchKeyword = localStorage.getItem('searchKey');
      }
      
    })
    
  }
  closeSelected(filter:any)
  {
    
    if(filter=='jl')
    {
      $('#job_level').val('');
      this.selectedJobLevel='';
      this.currentPage = 1;
      this.jobLevel = '';
      this.getCareerData(this.currentPage);
    }
    else if(filter=='js'){
      $('#job_sector').val('');
      this.selectedJobSector='';
      this.currentPage = 1;
      this.jobSector = '';
      this.getCareerData(this.currentPage);
    }
    else if(filter=='wg'){
      this.minWageSlider = this.fliterWageMin;
      this.maxWageSlider = this.filterWageMax;
      this.selectedWage = '';
      this.initJquery(this.minWageSlider, this.maxWageSlider);
      this.currentPage = 1;
      this.getCareerData(this.currentPage);
      
    }
  }
  GotoInteractive()
  {
    if(this.jobSector>0)
    {
      localStorage.setItem('jbSector', this.jobSector);
      localStorage.setItem('jbSectorName', this.selectedJobSector);
     // window.location.href='interactive-search';
      this._router.navigateByUrl('/interactive-search');
    }
    else
    {
      this._toastr.warning('Job Sector must be selected.');
    }
  }

  checkLogeedIn() {
    this._searchService.checkLoginState(this.apiKey, this.accessToken)
    .subscribe(res => {
      if(res.status != 200) { 
        localStorage.removeItem('access_token');
        localStorage.removeItem('user_name');
        localStorage.removeItem('user_role');
        localStorage.removeItem('user_id'); 
      }
      else
      {
        
        this.isLoggedInchk = true;
        let userName = localStorage.getItem('user_name');
        let userRole = localStorage.getItem('user_role');
        if(!this.isLoggedInchk)
        {
          this._afterLoginService.setLoggedInNameSubject('Register');
          this._afterLoginService.setLoggedOutNameSubject('Sign In'); 
        }else{
          if(userName!='' && userName!=undefined)
          {
            this._afterLoginService.setLoggedInNameSubject(userName); 
            this._afterLoginService.setLoggedOutNameSubject('Log Out');
            this.userRole = userRole; 
          }
          if(this.userRole == 1)
          {
            this._afterLoginService.setLoggedInUserRoleSubject('student');
          }  
          if(this.userRole == 2)
          {
            this._afterLoginService.setLoggedInUserRoleSubject('professional');
          }  
          
    
        }
      }
    },err => {
      console.log(err);
    });
    
  }



}
