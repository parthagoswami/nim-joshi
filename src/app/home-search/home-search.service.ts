import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HomeSearchService {

  constructor(private _http:HttpClient) { }
  getSearchSettingData(){
    return this._http.get(environment.apiBaseUrl + 'cms/search')
    .pipe(map(res => JSON.parse(JSON.stringify(res))));

  }
  getFilterCareerSearchOptions(accessToken: string) {
    let fd = new FormData();
    let headers = new HttpHeaders()
    //.set('Authorization', 'Bearer ' + accessToken);

    return this._http.post(environment.apiBaseUrl + 'get-searchresult-filter-options', fd, { headers })
    .pipe(map(res => JSON.parse(JSON.stringify(res))));
  }
  getCareerSearchData(accessToken: string, searchKeyword: any, jobLevel: any, jobSector:any,minWageSlider:any,maxWageSlider:any, userIdNo:any, pageNo: number)
  {
    //console.log(minWage);
    //console.log('test');
    let fd = new FormData();
    fd.append("keyword", searchKeyword);
    fd.append("sector_id", jobSector);
    fd.append("job_level_id", jobLevel);
    fd.append("min_wage", minWageSlider);
    fd.append("max_wage", maxWageSlider);
    fd.append("user_id", userIdNo);
  
    //let page_no = pageNo;
    //console.log(pageNo);
     fd.append("page", ''+pageNo);

    let headers = new HttpHeaders()
    //.set('Authorization', 'Bearer ' + accessToken);

    return this._http.post(environment.apiBaseUrl + 'searchresult-list', fd, { headers })
    .pipe(map(res => JSON.parse(JSON.stringify(res))));

  }
  allocateFavourite(apiKey: string, careeer_id: any, action_type: any,accessToken: string) {
    let fd = new FormData();
    //let fd = new FormData();
    fd.append("career_id", careeer_id);
    fd.append("action", action_type);

    // fd.append("password", formGroupData.password);

    let headers = new HttpHeaders()
    .set('Authorization', 'Bearer ' + accessToken);

    return this._http.post(environment.apiBaseUrl + 'user/user-career-tag', fd, { headers })
    .pipe(map(res => JSON.parse(JSON.stringify(res))));
  }
  checkLoginState(apiKey: string,accessToken: string){
    let fd = new FormData();

    let headers = new HttpHeaders()
    .set('Authorization', 'Bearer ' + accessToken);

    return this._http.post(environment.apiBaseUrl + 'auth/is-logged-in', fd, { headers: headers })
    .pipe(map(res => JSON.parse(JSON.stringify(res))));
  }
}
