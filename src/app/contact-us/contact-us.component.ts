import { Component, NgZone, OnInit } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ContactUsService } from './contact-us.service';
import { environment } from '../../environments/environment';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AfterLoginService } from '../after-login/after-login.service';
declare var $:any;

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {

  public pageData: any = [];
  public bannerUrl: any = [];
  public bannerCaption: any = [];
  public bannerAlt: string = '';
  public apiFileUrl: string = environment.apiFileUrl;
  public accessToken: string = '';
  public userRole: any;
  public isLoggedInchk : boolean = false;
  public contactForm: FormGroup = this.formBuilder.group({
    'contact_name': ['', [Validators.required]],
    'contact_email': ['', [Validators.required, Validators.pattern(/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i)]],
    'contact_no': ['', [Validators.required, Validators.pattern(/^((\\+91-?)|0)?[0-9]{10}$/)]],
    'contact_enquiry': ['',]

  })
  public isContactSaveClicked: boolean = false;
  public apiKey: string = environment.apiKey; 

  constructor(private _contactService:ContactUsService,private router:Router,private ngZone:NgZone,private _title:Title,private _toastr:ToastrService,private formBuilder:FormBuilder,private meta: Meta,private _afterLoginService:AfterLoginService) {
    let accessToken = localStorage.getItem(btoa('access_token'));
    if(typeof accessToken !== 'undefined' && accessToken != null && accessToken != '') {
      this.accessToken = atob(accessToken);
    }
    this.checkLogeedIn();
    
   }

  ngOnInit(): void {
    try{
      //let accessToken = localStorage.getItem(btoa('access_token'));
      this.getContactSettingData();
    }catch(error){
      console.log(error);
    }
    setTimeout(() =>{ $(".lazy").lazy(); }, 1000);
  }

  getContactSettingData(){    
    this._contactService.getContactUsSettingData()
    .subscribe((resp)=> {      
      if(resp.status == 200) {
        this.pageData = resp.data;
        this.bannerUrl =  resp.data.get_banner[0].banner_image;
        this.bannerCaption =  resp.data.get_banner[0].banner_image_caption;
        this.bannerAlt = resp.data.get_banner[0].title;
        this._title.setTitle(resp.data.page_meta_tag_title);
        this.meta.addTags([
          { name: 'keywords', content: resp.data.page_meta_tag_keywords },
          { name: 'description', content: resp.data.page_meta_tag_description },
          // ...
        ]);                   
        
      }
    },err => {
      console.log(err);
    });
  }

  public contactFormSubmitted(): void {
    this.isContactSaveClicked = true;
    if(this.contactForm.invalid) {
      //console.log('invalid');
      return;
      
    }
    $("#contactreqSubmit").attr('disabled', true);
    $('#contactreqSubmit').html('<i class="fa fa-spinner icon-spin"></i> Submit');
    this._contactService.contactFormSubmit(this.apiKey, this.contactForm.value)
    .subscribe(resp => {      
      $("#contactreqSubmit").removeAttr('disabled');
      $('#contactreqSubmit').html('Submit');
      if(resp.status == 200) {
       
        if(resp.contact_status==1)
        {
          this._toastr.success(resp.message); 
        }
        
        else{
          return;
        }
        
      }
    }, err => {     
      this._toastr.error('Some error occured.');
    });
    
    
    
  }
  checkLogeedIn() {
    this._contactService.checkLoginState(this.apiKey, this.accessToken)
    .subscribe(res => {
      if(res.status != 200) { 
        localStorage.removeItem('access_token');
        localStorage.removeItem('user_name');
        localStorage.removeItem('user_role');
        localStorage.removeItem('user_id'); 
      }
      else
      {
        
        this.isLoggedInchk = true;
        let userName = localStorage.getItem('user_name');
        let userRole = localStorage.getItem('user_role');
        if(!this.isLoggedInchk)
        {
          this._afterLoginService.setLoggedInNameSubject('Register');
          this._afterLoginService.setLoggedOutNameSubject('Sign In'); 
        }else{
          if(userName!='' && userName!=undefined)
          {
            this._afterLoginService.setLoggedInNameSubject(userName); 
            this._afterLoginService.setLoggedOutNameSubject('Log Out');
            this.userRole = userRole; 
          }
          if(this.userRole == 1)
          {
            this._afterLoginService.setLoggedInUserRoleSubject('student');
          }  
          if(this.userRole == 2)
          {
            this._afterLoginService.setLoggedInUserRoleSubject('professional');
          }  
          

        }
      }
    },err => {
      console.log(err);
    });
    
  }
  

}
