import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ContactUsService {

  constructor(private _http:HttpClient) { }
  getContactUsSettingData(){
    return this._http.get(environment.apiBaseUrl + 'cms/contact-us')
    .pipe(map(res => JSON.parse(JSON.stringify(res))));
  }
  contactFormSubmit(apiKey: string, formGroupData: any)
  {
    let fd = new FormData();
    
    fd.append("name", formGroupData.contact_name);
    fd.append("email", formGroupData.contact_email);
    fd.append("contact_no", formGroupData.contact_no);
    fd.append("enquiry", formGroupData.contact_enquiry);
   
    let headers = new HttpHeaders()
    .set('X-Auth-Token', apiKey);    

    return this._http.post(environment.apiBaseUrl + 'auth/contact', fd, { headers })
    .pipe(map(res => JSON.parse(JSON.stringify(res))));

  }
  checkLoginState(apiKey: string,accessToken: string){
    let fd = new FormData();

    let headers = new HttpHeaders()
    .set('Authorization', 'Bearer ' + accessToken);

    return this._http.post(environment.apiBaseUrl + 'auth/is-logged-in', fd, { headers: headers })
    .pipe(map(res => JSON.parse(JSON.stringify(res))));
  }
}
