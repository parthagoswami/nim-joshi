import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { APP_BASE_HREF } from '@angular/common';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { MyProfileComponent } from './after-login/my-profile/my-profile.component';
import { AfterLoginComponent } from './after-login/after-login.component';
import { GuideMeComponent } from './after-login/guide-me/guide-me.component';
import { ExploreCareerComponent } from './after-login/explore-career/explore-career.component';
import { CareerSearchComponent } from './after-login/career-search/career-search.component';
import { CareerGoalComponent } from './after-login/career-goal/career-goal.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { AboutUsComponent } from './about-us/about-us.component';
import { HowItWorksComponent } from './how-it-works/how-it-works.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { HomeSearchComponent } from './home-search/home-search.component';
import { CareerDetailsComponent } from './career-details/career-details.component';
import { InteractiveSearchComponent } from './interactive-search/interactive-search.component';
import { TermsConditionsComponent } from './terms-conditions/terms-conditions.component';
import { FaqsComponent } from './faqs/faqs.component';
import { ChangePasswordComponent } from './after-login/change-password/change-password.component';
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    PageNotFoundComponent,
    RegisterComponent,
    LoginComponent,
    MyProfileComponent,
    AfterLoginComponent,
    GuideMeComponent,
    ExploreCareerComponent,
    CareerSearchComponent,
    CareerGoalComponent,
    AboutUsComponent,
    HowItWorksComponent,
    ContactUsComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent,
    HomeSearchComponent,
    CareerDetailsComponent,
    InteractiveSearchComponent,
    TermsConditionsComponent,
    FaqsComponent,
    ChangePasswordComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    NgMultiSelectDropDownModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
