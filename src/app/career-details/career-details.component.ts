import { Component, NgZone, OnInit } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { CareerDetailsService } from './career-details.service';
import { environment } from '../../environments/environment';
import { AfterLoginService } from '../after-login/after-login.service';
import { Location } from '@angular/common';
declare var $:any;

@Component({
  selector: 'app-career-details',
  templateUrl: './career-details.component.html',
  styleUrls: ['./career-details.component.css']
})
export class CareerDetailsComponent implements OnInit {
  public pageData: any = [];
  public bannerUrl: any = [];
  public bannerCaption: any = [];
  public bannerAlt: string = '';
  public apiFileUrl: string = environment.apiFileUrl;
  public userIdNo : any = [];
  public accessToken: string = '';
  public isLoggedIn : boolean=false;
  public careerSlug: string = '';
  public careerData: any = [];
  public userRole: any;
  public apiKey: string = environment.apiKey;
  public isLoggedInchk : boolean = false; 
  constructor(private _cardetailService:CareerDetailsService,private router:Router,private ngZone:NgZone,private _title:Title,private _toastr:ToastrService,private _activatedRoute:ActivatedRoute,private meta: Meta,private _afterLoginService:AfterLoginService, private location:Location) { 
   let accessToken = localStorage.getItem(btoa('access_token'));
    if(typeof accessToken !== 'undefined' && accessToken != null && accessToken != '') {
      this.accessToken = atob(accessToken);
    }
    this.checkLogeedIn();
    
   
  }

  ngOnInit(): void {
    try{
     
      let accessToken = localStorage.getItem(btoa('access_token'));
      if(typeof accessToken !== 'undefined' && accessToken != null && accessToken != '') {
        this.accessToken = atob(accessToken); 
        this.isLoggedIn = true;
      }
      else
      {
        localStorage.removeItem('user_id');
      }
      let userId = localStorage.getItem('user_id');
      if(typeof userId !== 'undefined' && userId != null && userId != '')
      {
        this.userIdNo = btoa(userId);
      }
      this.careerSlug = this._activatedRoute.snapshot.params.slug;
      this.getdetailSettingData();
      this.getCareerData();
    }catch(error){
      console.log(error);
    }
    //setTimeout(() =>{ this.initJquery(); }, 1000);
  }
  getdetailSettingData(){    
    this._cardetailService.getcarDetailSettingData()
    .subscribe((resp)=> {      
      if(resp.status == 200) {
        this.pageData = resp.data;
        this.bannerUrl =  resp.data.get_banner[0].banner_image;
        this.bannerCaption =  resp.data.get_banner[0].banner_image_caption;
        this.bannerAlt = resp.data.get_banner[0].title;
        this._title.setTitle(resp.data.page_meta_tag_title); 
        this.meta.addTags([
          { name: 'keywords', content: resp.data.page_meta_tag_keywords },
          { name: 'description', content: resp.data.page_meta_tag_description },
          // ...
        ]);              
        //console.log(resp);
      }
    },err => {
      console.log(err);
    });
  }

  getCareerData() {
    
    this._cardetailService.getCareerDetailData(this.accessToken, this.careerSlug,this.userIdNo)
    .subscribe((resp) => {
    
      if(resp.status==200)
      {
        this.careerData = resp.data;
        //console.log(this.careerData);
        setTimeout(() =>{ this.progressbar(this.careerData); }, 1000);
       
      }      
    }, err => {
      console.log(err);
    });
  }
  progressbar(career:any)
  {
  
    var wage_match = parseFloat(career.wage_match)/100;
    var int_match = parseFloat(career.interested_match)/100;
    var value_match = parseFloat(career.value_match)/100;
 
    $(".radialProgressBar #progress_wage").circleProgress({
      startAngle: -1.55,
      size: 200,
      value: wage_match,
      fill: {gradient: ['#f8b914']}
    });
    
    $(".radialProgressBar #progress_interest").circleProgress({
      startAngle: -1.55,
      size: 200,
      value: int_match,
      fill: {gradient: ['#f8b914']}
    });
    $(".radialProgressBar #progress_value").circleProgress({
      startAngle: -1.55,
      size: 200,
      value: value_match,
      fill: {gradient: ['#f8b914']}
    });
    
    
  }
  AddFavorite(values:any):void {
    //console.log(values);
    var career_id = values;
    var action_type = 'add';
    this._cardetailService.allocateFavourite(this.apiKey, career_id, action_type, this.accessToken)
    .subscribe(resp => {      
      
      if(resp.status == 200) {
       
        if(resp.response!='')
        {
          this._toastr.success(resp.response); 
          this.getCareerData();
        }
        
        
        else{
          return;
        }
        
      }
    }, err => {     
      this._toastr.error('Some error occured.');
    });
  }
  removeFavorite(values:any):void {
    var career_id = values;
    var action_type = 'remove';
    this._cardetailService.allocateFavourite(this.apiKey, career_id, action_type, this.accessToken)
    .subscribe(resp => {      
      
      if(resp.status == 200) {
       
        if(resp.response!='')
        {
          this._toastr.success(resp.response); 
          this.getCareerData();
         
        }
        
        
        else{
          return;
        }
        
      }
    }, err => {     
      this._toastr.error('Some error occured.');
    });
  }

  checkLogeedIn() {
    this._cardetailService.checkLoginState(this.apiKey, this.accessToken)
    .subscribe(res => {
      if(res.status != 200) { 
        localStorage.removeItem('access_token');
        localStorage.removeItem('user_name');
        localStorage.removeItem('user_role');
        localStorage.removeItem('user_id'); 
      }
      else
      {
        console.log(res);
        this.isLoggedInchk = true;
        let userName = localStorage.getItem('user_name');
        let userRole = localStorage.getItem('user_role');
        if(!this.isLoggedInchk)
        {
          this._afterLoginService.setLoggedInNameSubject('Register');
          this._afterLoginService.setLoggedOutNameSubject('Sign In'); 
        }else{
          if(userName!='' && userName!=undefined)
          {
            this._afterLoginService.setLoggedInNameSubject(userName); 
            this._afterLoginService.setLoggedOutNameSubject('Log Out');
            this.userRole = userRole; 
          }
          if(this.userRole == 1)
          {
            this._afterLoginService.setLoggedInUserRoleSubject('student');
          }  
          if(this.userRole == 2)
          {
            this._afterLoginService.setLoggedInUserRoleSubject('professional');
          }  
          

        }
      }
    },err => {
      console.log(err);
    });
    
  }
  back(): void {
    this.location.back();
  }
  

}
