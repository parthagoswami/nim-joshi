import { TestBed } from '@angular/core/testing';

import { CareerDetailsService } from './career-details.service';

describe('CareerDetailsService', () => {
  let service: CareerDetailsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CareerDetailsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
