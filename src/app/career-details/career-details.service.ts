import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CareerDetailsService {

  constructor(private _http:HttpClient) { }
  getcarDetailSettingData(){
    return this._http.get(environment.apiBaseUrl + 'cms/career-details')
    .pipe(map(res => JSON.parse(JSON.stringify(res))));

  }
  
  getCareerDetailData(accessToken: string, searchKcareerSlug: any, userId: any)
  {
    
    let fd = new FormData();
    fd.append("slug", searchKcareerSlug);
    fd.append("user_id", userId);
    let headers = new HttpHeaders()
    //.set('Authorization', 'Bearer ' + accessToken);

    return this._http.post(environment.apiBaseUrl + 'searchresult-details', fd, { headers })
    .pipe(map(res => JSON.parse(JSON.stringify(res))));

  }
  allocateFavourite(apiKey: string, careeer_id: any, action_type: any,accessToken: string) {
    let fd = new FormData();
    //let fd = new FormData();
    fd.append("career_id", careeer_id);
    fd.append("action", action_type);

    // fd.append("password", formGroupData.password);

    let headers = new HttpHeaders()
    .set('Authorization', 'Bearer ' + accessToken);

    return this._http.post(environment.apiBaseUrl + 'user/user-career-tag', fd, { headers })
    .pipe(map(res => JSON.parse(JSON.stringify(res))));
  }
  checkLoginState(apiKey: string,accessToken: string){
    let fd = new FormData();

    let headers = new HttpHeaders()
    .set('Authorization', 'Bearer ' + accessToken);

    return this._http.post(environment.apiBaseUrl + 'auth/is-logged-in', fd, { headers: headers })
    .pipe(map(res => JSON.parse(JSON.stringify(res))));
  }
}
