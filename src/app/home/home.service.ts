import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  constructor(private _http: HttpClient) { }
  getHomePageData(){
    let fd = new FormData();
    // let headers = new HttpHeaders()
    // .set('Authorization', 'Bearer ' + accessToken);

    return this._http.get(environment.apiBaseUrl + 'cms/home')
    .pipe(map(res => JSON.parse(JSON.stringify(res))));
  }
  checkLoginState(apiKey: string,accessToken: string){
    let fd = new FormData();

    let headers = new HttpHeaders()
    .set('Authorization', 'Bearer ' + accessToken);
    return this._http.post(environment.apiBaseUrl + 'auth/is-logged-in', fd, { headers: headers })
    .pipe(map(res => JSON.parse(JSON.stringify(res))));
  }
}
