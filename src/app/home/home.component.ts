import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HomeService } from './home.service';
import { environment } from '../../environments/environment';
import { Pipe, PipeTransform } from '@angular/core';
import { Title, Meta  } from '@angular/platform-browser';
import { AfterLoginService } from '../after-login/after-login.service';
declare var $:any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {

  public accessToken: string = '';
  public pageData: any = [];
  public apiFileUrl: string = environment.apiFileUrl;
  public bannerUrl: string = '';
  public bannerCaption: any = '';
  public bannerLink: any = '';
  public userRole: any;
  public apiKey: string = environment.apiKey;
  public isLoggedInchk : boolean = false;
  constructor(private _homeService: HomeService, private _router: Router, private _title: Title,private meta: Meta,private _afterLoginService:AfterLoginService) {
    let accessToken = localStorage.getItem(btoa('access_token'));
    if(typeof accessToken !== 'undefined' && accessToken != null && accessToken != '') {
      this.accessToken = atob(accessToken);
    }
    this.checkLogeedIn();
 
   }

    ngOnInit(): void {
      try{
        
        this.getPageData();
      }catch(error){
        console.log(error);
      }
      setTimeout(() =>{ this.initJquery(); }, 1000);
    }

    getPageData(){    
      this._homeService.getHomePageData()
      .subscribe((resp)=> {      
        if(resp.status == 200) {
          this.pageData = resp.data; 
          this.bannerUrl =  resp.data.get_banner[0].banner_image;
          this.bannerCaption =  resp.data.get_banner[0].banner_image_caption;
          this.bannerLink =  resp.data.get_banner[0].banner_url;
          this._title.setTitle(resp.data.page_meta_tag_title);
          this.meta.addTags([
            { name: 'keywords', content: resp.data.page_meta_tag_keywords },
            { name: 'description', content: resp.data.page_meta_tag_description },
            // ...
          ]);
          //console.log(resp);
          //this.initJquery();       
          
        }
      },err => {
        console.log(err);
      });
    }

    initJquery(){
      $(".lazy").lazy();
      //console.log('test');
      let howitworkSlideOwl = $('.howitwork-slider').owlCarousel({
        loop: true,
        margin: 0,
        nav: false,
        autoplay: true,
        autoplayTimeout: 2000,
        smartSpeed: 500,
        autoplayHoverPause: true,
        dots: true,
        dotsContainer: '#howitwork-custom-dots',
        dotsData: true,
        responsive:{
            0:{ items: 1 },
            600:{ items: 1 },
            1000:{ items: 1 }
        },
        lazyLoad: true
    });
    $('.owl-dot').click(() => {
        howitworkSlideOwl.trigger('to.owl.carousel', [$(this).index(), 1000]);
    })
      let selfexplrSlideOwl = $('.selfexplr-slider-wrap').owlCarousel({
        loop: false,
        margin: 0,
        nav: false,
        autoplay: true,
        autoplayTimeout: 2000,
        smartSpeed: 500,
        autoplayHoverPause: true,
        dots: true,
        // dotsData: true,
        responsive:{
            0:{ items: 1 },
            600:{ items: 2 },
            1000:{ items: 3 }
        },
        lazyLoad: true
    });
  }

  checkLogeedIn() {

    this._homeService.checkLoginState(this.apiKey, this.accessToken)
    .subscribe(res => {
      
      if(res.status != 200) { 
        localStorage.removeItem('access_token');
        localStorage.removeItem('user_name');
        localStorage.removeItem('user_role');
        localStorage.removeItem('user_id'); 
      }
      else
      {
        
        this.isLoggedInchk = true;
        let userName = localStorage.getItem('user_name');
        let userRole = localStorage.getItem('user_role');
       
        if(!this.isLoggedInchk)
        {
          this._afterLoginService.setLoggedInNameSubject('Register');
          this._afterLoginService.setLoggedOutNameSubject('Sign In');
        }else{
          if(userName!='' && userName!=undefined)
          {
            this._afterLoginService.setLoggedInNameSubject(userName); 
            this._afterLoginService.setLoggedOutNameSubject('Log Out');
            this.userRole = userRole; 
          }
          if(this.userRole == 1)
          {
            this._afterLoginService.setLoggedInUserRoleSubject('student');
          }  
          if(this.userRole == 2)
          {
            this._afterLoginService.setLoggedInUserRoleSubject('professional');
          }  
          
    
        }
      }
    },err => {
      console.log(err);
    });
    
  }
  

}
