import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(private _http: HttpClient) { }
  getRegisterSettingData(){
    let fd = new FormData();
    // let headers = new HttpHeaders()
    // .set('Authorization', 'Bearer ' + accessToken);

    return this._http.get(environment.apiBaseUrl + 'cms/register')
    .pipe(map(res => JSON.parse(JSON.stringify(res))));
  }
  duplicateEmail(apiKey: string, formGroupData: any) {
    let fd = new FormData();
    fd.append("email", formGroupData.email);
    
    let headers = new HttpHeaders()
    .set('X-Auth-Token', apiKey);    

    return this._http.post(environment.apiBaseUrl + 'auth/duplicate-email', fd, { headers })
    .pipe(map(res => JSON.parse(JSON.stringify(res))));
  }
  registerSubmit(apiKey: string, formGroupDataEmail: any, formGroupDataPersonal: any, formGroupDataEducation: any,formGroupDataExperience: any) {
    console.log(formGroupDataEmail);
    console.log(formGroupDataPersonal);
    console.log(formGroupDataEducation);
    console.log(formGroupDataExperience);
    let fd = new FormData();
    let full_name = formGroupDataPersonal.first_name+' '+formGroupDataPersonal.last_name;
    fd.append("first_name", formGroupDataPersonal.first_name);
    fd.append("last_name", formGroupDataPersonal.last_name);
    fd.append("email", formGroupDataEmail.email);
    fd.append("email", formGroupDataEmail.email);
    fd.append("password", formGroupDataEmail.password);
    fd.append("role_id", formGroupDataEmail.user_type);
    fd.append("name", full_name);

    fd.append("gender", formGroupDataPersonal.gender);
    fd.append("contact_no", formGroupDataPersonal.contact_no);
    fd.append("alternate_email", formGroupDataPersonal.email_altername);
    fd.append("address", formGroupDataPersonal.address);

    fd.append("qualification_title", formGroupDataEducation.qualificaion);
    fd.append("grade", formGroupDataEducation.grade_quali);

    fd.append("company_name", formGroupDataExperience.employer_name);
    fd.append("work_year", formGroupDataExperience.exp_years);
    fd.append("work_month", formGroupDataExperience.exp_months);
    fd.append("designation", formGroupDataExperience.job_title);
    
    let headers = new HttpHeaders()
    .set('X-Auth-Token', apiKey);    

    return this._http.post(environment.apiBaseUrl + 'auth/register', fd, { headers })
    .pipe(map(res => JSON.parse(JSON.stringify(res))));
  }

}
