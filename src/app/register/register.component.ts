import { Component, NgZone, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RegisterService } from './register.service';
import { environment } from '../../environments/environment';
import { Title, Meta } from '@angular/platform-browser';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
declare var $:any;

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  public accessToken: string = '';
  public pageData: any = [];
  public bannerUrl: any = [];
  public bannerCaption: any = [];
  public bannerAlt: string = '';
  public apiFileUrl: string = environment.apiFileUrl;
  public ShowHideVal:string = 'showworkexp';
  
 

  public registerForm: FormGroup = this.formBuilder.group({
    'user_type': ['', [Validators.required]],
    'email': ['', [Validators.required, Validators.pattern(/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i)]],
    'password': ['', [Validators.required, Validators.pattern(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$/)]],
  })
  public personalForm: FormGroup = this.formBuilder.group({
    'first_name': ['', [Validators.required, Validators.pattern(/^([a-zA-Z]+\s)*[a-zA-Z]+$/)]],
    'last_name': ['', [Validators.required, Validators.pattern(/^([a-zA-Z]+\s)*[a-zA-Z]+$/)]],
    'gender': ['', [Validators.required]],
    'email_altername': ['', [Validators.pattern(/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i)]],
    'contact_no': ['', [Validators.required, Validators.pattern(/^((\\+91-?)|0)?[0-9]{10}$/)]],
    'address': ['', [Validators.required]],
  })
  public educationForm: FormGroup = this.formBuilder.group({
    'qualificaion': ['', [Validators.required]],
    'grade_quali': ['', [Validators.required]],
    
  })
  public experienceForm: FormGroup = this.formBuilder.group({
    'job_title': ['', [Validators.required]],
    'employer_name': ['', [Validators.required]],
    'exp_years': ['', [Validators.required]],
    'exp_months': ['', [Validators.required]],
    
  })

  public isRegisterTab: boolean = true;
  public isPersonalTab: boolean = false;
  public isEducationTab: boolean = false;
  public isExperienceTab: boolean = false;
  public isSaveClicked: boolean = false;
  public isStepEmail: boolean = false;
  public isStepPersonal: boolean = false;
  public isStepEduction: boolean = false;
  public isStepExperience: boolean = false;
  public UserType: any = [];
  public isPersonalSaveClicked: boolean = false;
  public isEductionSaveClicked: boolean = false;
  public isExperienceSaveClicked: boolean = false;
  public EducationButtonText: any = ['Submit'];
  public apiKey: string = environment.apiKey; 
  public duplicateEmailStat: any = [];
  public duplicateEmailMsg: string = '';
  public alternateEmailStat: any = [];
  public alternateEmailMsg: string = '';
  public isRegistered: boolean = false;



  constructor(private _registerService: RegisterService, private _router: Router, private ngZone:NgZone, private _title: Title, private formBuilder:FormBuilder, private _toastr:ToastrService,private meta: Meta) { }

  ngOnInit(): void {
    try{
      //let accessToken = localStorage.getItem(btoa('access_token'));
      this.getRegSettingData();
    }catch(error){
      console.log(error);
    }
    
    setTimeout(() =>{ $(".lazy").lazy(); }, 1000);
    $('#hideshowpwd').on('click', function(){
      console.log('hideshowpwd');
      let current_tp = $('#passwordval').attr('type');
      if(current_tp=='text')
      {
        $('#passwordval').attr('type','password');
        $('#hideshowpwd').html('<i class="fa fa-eye-slash" aria-hidden="true"></i>');
      }
      else
      {
        $('#passwordval').attr('type','text');
        $('#hideshowpwd').html('<i class="fa fa-eye" aria-hidden="true"></i>');
      }
      
    });
  }
  getRegSettingData(){    
    this._registerService.getRegisterSettingData()
    .subscribe((resp)=> {      
      if(resp.status == 200) {
        this.pageData = resp.data;
        this.bannerUrl =  resp.data.get_banner[0].banner_image;
        this.bannerCaption =  resp.data.get_banner[0].banner_image_caption;
        this.bannerAlt = resp.data.get_banner[0].title;
        this._title.setTitle(resp.data.page_meta_tag_title); 
        this.meta.addTags([
          { name: 'keywords', content: resp.data.page_meta_tag_keywords },
          { name: 'description', content: resp.data.page_meta_tag_description },
          // ...
        ]);         
        
      }
    },err => {
      console.log(err);
    });
  }
  counter(i: number) {
    return new Array(i);
  }
  HideShowWorkExp(value:string)
  {
    $(".usertypecls").removeClass("checked");
    if(parseInt(value)==1)
    {
      this.ShowHideVal = 'hideworkexp';
      this.EducationButtonText = 'Submit';
      this.UserType = 1;
      $("#studenttype").parent(".form-check-label").addClass("checked");
    }
    else
    {
      this.ShowHideVal = 'showworkexp';
      this.EducationButtonText = 'Next';
      this.UserType = 2;
      $("#professionaltype").parent(".form-check-label").addClass("checked");

    }
    console.log(value);
  }
  SelectGender(value:string)
  {
    
    $(".gendercls").removeClass("checked");
    if(parseInt(value)==1)
    {
      $("#gender_male").parent(".form-check-label").addClass("checked");
    }
    else if(parseInt(value)==2)
    {
      $("#gender_female").parent(".form-check-label").addClass("checked");
    }
    else
    {
      $("#gender_binary").parent(".form-check-label").addClass("checked");
    }
  }

  changeTab(e:any)
  {    
    
    console.log(e);
    if(parseInt(e)==1)
    {
      this.isStepEmail = false;
      this.isRegisterTab = true;
      this.isPersonalTab = false; 
      this.isEducationTab = false;
      this.isExperienceTab = false; 
    }else if(parseInt(e)==2){
      if(this.isStepEmail)
      {console.log(1);
        this.isStepPersonal = false;
        this.isRegisterTab = false;
        this.isPersonalTab = true;  
        this.isEducationTab = false;
        this.isExperienceTab = false; 
      }
      
    }else if(parseInt(e)==3){
      if(this.isStepPersonal)
      {
        this.isStepEduction = false;
        this.isRegisterTab = false;
        this.isPersonalTab = false;  
        this.isEducationTab = true;
        this.isExperienceTab = false;
      }
       
    }else{
      if(this.isStepEduction)
      {
        this.isStepExperience = false;
        this.isRegisterTab = false;
        this.isPersonalTab = false;  
        this.isEducationTab = false;
        this.isExperienceTab = true;
      }
       
    }
  }
  public registerFormSubmitted(): void {
    
    this.isSaveClicked = true;
    this.duplicateEmailStat = 0;
    this.isStepEmail = false;
   
    if(this.registerForm.invalid) {
      //console.log('invalid');
      return;
      
    }
    $('#basicnextbtn').attr('disabled','true');
    $('#basicnextbtn').html('<i class="fa fa-spinner icon-spin"></i> Next');
    this._registerService.duplicateEmail(this.apiKey, this.registerForm.value)
    .subscribe(resp => {      
     
      if(resp.status == 200) {
        console.log(resp.data);
        this.duplicateEmailMsg = resp.data.message;
        this.duplicateEmailStat = resp.data.status;
        if(resp.data.message!='')
        {
          this._toastr.error(resp.data.message,'Duplicate Email'); 
        }
        
        if(this.duplicateEmailStat==0)
        {
          $('#basicnextbtn').removeAttr('disabled');
          $('#basicnextbtn').html('Next');
          this.isStepEmail = true;
          this.isRegisterTab = false;
          this.isPersonalTab = true;  
          this.isEducationTab = false;
          this.isExperienceTab = false;
        }
        else{
          $('#basicnextbtn').removeAttr('disabled');
          $('#basicnextbtn').html('Next');
          return;
        }
        
      }
    }, err => {     
      this._toastr.error(err.message);
    });
    
    
       
    
    
    
  }
  public personalFormSubmitted(): void {
    
   
    this.isPersonalSaveClicked = true;
    
    if(this.personalForm.invalid) {
      return;
    }
    $('#personalnextbtn').attr('disabled','true');
    $('#personalnextbtn').html('<i class="fa fa-spinner icon-spin"></i> Next');
    if(this.registerForm.value.email==this.personalForm.value.email_altername)
    {
      this.alternateEmailStat=1;
      this.alternateEmailMsg='Altername Email can not be same as primary email.';
      this._toastr.error(this.alternateEmailMsg);
    }
    else{
      $('#personalnextbtn').removeAttr('disabled');
      $('#personalnextbtn').html('Next');
      this.alternateEmailStat=0;
      this.alternateEmailMsg='';
      this.isRegisterTab = false;
      this.isPersonalTab = false;  
      this.isEducationTab = true;
      this.isExperienceTab = false;

    }
    
  }
  public eductionFormSubmitted(): void {
   
    this.isEductionSaveClicked = true;
    if(this.educationForm.invalid) {
      return;
    }
    if(this.UserType ==2){
      this.isRegisterTab = false;
      this.isPersonalTab = false;  
      this.isEducationTab = false;
      this.isExperienceTab = true;
    }
    else{
      
      $("#eduSubmit").html("<i class='fa fa-spinner fa-spin'></i>");
      $("#eduSubmit").attr('disabled', true);
      this._registerService.registerSubmit(this.apiKey, this.registerForm.value,  this.personalForm.value, this.educationForm.value, this.experienceForm.value)
      .subscribe(resp => {      
      $("#eduSubmit").removeAttr('disabled');
      console.log(resp);
      if(resp.status == 200) {  
        if(resp.errors!=undefined)
        {
          let errMsg = '';
          $.each(resp.errors, function(name:any, value:any){   
            if(errMsg=='')
            {
              errMsg = value; 
            }
            else
            {
              errMsg = ', '+value; 
            }
                  
            //errMsg = 'Some Error Occured.';
          })
          this._toastr.error(errMsg); 
        }  
        else{
          this.isRegistered = true;          
          //localStorage.setItem(btoa('access_token'), btoa(resp.access_token));
          //localStorage.setItem('userInfo', JSON.stringify(resp.user));
          //this.accessToken = resp.access_token;
          this._toastr.success('Registered Successfully.');
          this._router.navigate(['/index']);

        }
        
        //this._router.navigate(['/index']);
       
      }
      else {       
        this._toastr.error('Something went wrong.'); 
      }
    }, err => {   
      $("#eduSubmit").removeAttr('disabled');        
      //this._toastr.error(err.message);
      this._toastr.error('Something went wrong.');
    });
    }
  }
  public experienceFormSubmitted(): void {
    
    this.isExperienceSaveClicked = true;
    if(this.experienceForm.invalid) {
      return;
    }
    
    $("#expSubmit").attr('disabled', true);
    $('#expSubmit').html('<i class="fa fa-spinner icon-spin"></i> Submit');
    this._registerService.registerSubmit(this.apiKey, this.registerForm.value,  this.personalForm.value, this.educationForm.value, this.experienceForm.value)
    .subscribe(resp => {      
    $("#expSubmit").removeAttr('disabled');
    $('#expSubmit').html('Submit');
    if(resp.status == 200) { 
      if(resp.errors!=undefined)
      {
        let errMsg = '';
        $.each(resp.errors, function(name:any, value:any){   
          if(errMsg=='')
          {
            errMsg = value; 
          }
          else
          {
            errMsg = ', '+value; 
          }
                
          
        })
        this._toastr.error(errMsg); 
      } 
      else
      {
        this.isRegistered = true;         
        localStorage.setItem(btoa('access_token'), btoa(resp.access_token));
        localStorage.setItem('userInfo', JSON.stringify(resp.user));
        this.accessToken = resp.access_token;
        this._toastr.success('Registered Successfully.');
        this._router.navigate(['/index']);
      }   
      
     
    }
  }, err => {   
    $("#expSubmit").removeAttr('disabled');        
    this._toastr.error('Something went wrong.');
  });
  }

}

