import { TestBed } from '@angular/core/testing';

import { AuthProfessionalService } from './auth-professional.service';

describe('AuthProfessionalService', () => {
  let service: AuthProfessionalService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AuthProfessionalService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
