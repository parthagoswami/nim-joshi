import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HeaderService } from '../header/header.service';
import { environment } from '../../environments/environment';
import {DomSanitizer} from '@angular/platform-browser';
declare var $:any;

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  public accessToken: string = '';
  public settingData: any = [];
  public apiFileUrl: string = environment.apiFileUrl;
  public sanitizedUrl: any = [];
  public faxData: string = '';

  constructor(private _headerService: HeaderService, private _router: Router,private sanitizer:DomSanitizer) { }

  ngOnInit(): void {
    try{
      //let accessToken = localStorage.getItem(btoa('access_token'));
      this.getSettingData();
    }catch(error){
      console.log(error);
    }
    $(".lazy").lazy();
  }
  getSettingData(){    
    this._headerService.getCommonSettingData()
    .subscribe((resp)=> {      
      if(resp.status == 200) {
        this.settingData = resp.data;        
        //console.log(resp);
        this.faxData = 'fax:'+resp.data.fax;
      }
    },err => {
      console.log(err);
    });
  }
  sanitize(url:string){
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }

}
