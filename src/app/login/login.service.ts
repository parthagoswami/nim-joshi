import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private _http:HttpClient) { }
  getLoginSettingData(){
    let fd = new FormData();
    // let headers = new HttpHeaders()
    // .set('Authorization', 'Bearer ' + accessToken);

    return this._http.get(environment.apiBaseUrl + 'cms/login')
    .pipe(map(res => JSON.parse(JSON.stringify(res))));
  }
  loginFormSubmit(apiKey: string,formGroupData: any) {
    let fd = new FormData();
    fd.append("email", formGroupData.email);    
    fd.append("password", formGroupData.password);
    let headers = new HttpHeaders()
    .set('X-Auth-Token', apiKey);
    return this._http.post(environment.apiBaseUrl + 'auth/login', fd, { headers })
    .pipe(map(res => JSON.parse(JSON.stringify(res))));
  }
}
