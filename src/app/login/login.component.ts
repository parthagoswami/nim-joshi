import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Title, Meta } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { LoginService } from './login.service';
import { environment } from '../../environments/environment';
import { ToastrService } from 'ngx-toastr';
declare var $:any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public pageData: any = [];
  public apiFileUrl: string = environment.apiFileUrl;
  public bannerUrl: any = [];
  public bannerCaption: any = [];
  public bannerAlt: string = '';
  public isLoginClicked: boolean = false;
  public apiKey: string = environment.apiKey; 
  public invalidPwd: boolean = false;
  public loginError: string = '';
 

  public loginForm: FormGroup = this.formBuilder.group({
    'email': ['', [Validators.required, Validators.pattern(/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i)]],
    'password': ['', [Validators.required, Validators.pattern(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$/)]],
    
  })

  constructor(private _loginService: LoginService, private _title: Title, private formBuilder: FormBuilder, private _router:Router, private _toastr:ToastrService,private meta: Meta) { }

  ngOnInit(): void {
    try{
     
      this.getLogSettingData();
    }catch(error){
      console.log(error);
    }
    
    setTimeout(() =>{ $(".lazy").lazy(); }, 1000);
    $('#hideshowpwd').on('click', function(){
      
      let current_tp = $('#password').attr('type');
      if(current_tp=='text')
      {
        $('#password').attr('type','password');
        $('#hideshowpwd').html('<i class="fa fa-eye-slash" aria-hidden="true"></i>');
      }
      else
      {
        $('#password').attr('type','text');
        $('#hideshowpwd').html('<i class="fa fa-eye" aria-hidden="true"></i>');
      }
      
    });
  }

  getLogSettingData(){    
    this._loginService.getLoginSettingData()
    .subscribe((resp)=> {      
      if(resp.status == 200) {
        this.pageData = resp.data;
        this.bannerUrl =  resp.data.get_banner[0].banner_image;
        this.bannerCaption =  resp.data.get_banner[0].banner_image_caption;
        this.bannerAlt = resp.data.get_banner[0].title;
        this._title.setTitle(resp.data.page_meta_tag_title); 
        this.meta.addTags([
          { name: 'keywords', content: resp.data.page_meta_tag_keywords },
          { name: 'description', content: resp.data.page_meta_tag_description },
          // ...
        ]);         
        //console.log(resp);
        //console.log('testlog');
      }
    },err => {
      console.log(err);
    });
  }
  public LoginFormSubmitted(): void {
    this.isLoginClicked = true;
    this.invalidPwd = false;
    if(this.loginForm.invalid) {
      return;
    }
    $('#logsubmitbtn').attr('disabled','true');
    $('#logsubmitbtn').html('<i class="fa fa-spinner icon-spin"></i> Submit');
    this._loginService.loginFormSubmit(this.apiKey, this.loginForm.value)
    .subscribe(resp => {
      $('#logsubmitbtn').removeAttr('disabled');
      $('#logsubmitbtn').html('Submit');
      //console.log(resp);
      if(resp.status == 200) {
        if(resp.errors == undefined) {
          localStorage.setItem(btoa('access_token'), btoa(resp.access_token));
          localStorage.setItem('user_id', resp.id);
          localStorage.setItem('user_name', resp.user.name);
          localStorage.setItem('user_role', resp.user.role_id);
          localStorage.setItem('profile_image', resp.user.profile_image);
          this._toastr.success('Logged In Successfully'); 
          
          if(resp.user.role_id==1)
          {
            this._router.navigateByUrl('/student/my-profile'); 
          }
          if(resp.user.role_id==2)
          {
            this._router.navigateByUrl('/professional/my-profile'); 
          }
         
        } else {  

         
            this.invalidPwd = true;
            this.loginError = resp.errors;
            setTimeout(()=>{                        
              this.invalidPwd = false;
              this.loginError = '';
          }, 3000);
          
          //this._toastr.error(resp.errors); 
          this._toastr.error(resp.errors, undefined, {
            positionClass: 'toast-bottom-center'
       });
        }            
      } else {       
        //this._toastr.error(resp.errors); 
        this._toastr.error('Something went wrong.');
      }


    }, err => {
      console.log(err);
      //this.loginMsg = err.message;
      this._toastr.error('Something went wrong.');
    });
    

  }

}
