export const environment = {
  production: true,
  apiBaseUrl: 'http://developer-beta.com/p2/NimJyoshi/api/',
  apiFileUrl: 'http://developer-beta.com/p2/NimJyoshi/',
  apiKey: 'testkey1',
  profileTitle: "Career Pathways Tool - My Profile",
  goalTitle: "Career Pathways Tool - Career Goals",
  changePwdTitle: "Career Pathways Tool - Change Password",
};
